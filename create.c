#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/inotify.h>
#include <linux/types.h>

#define EVENT_SIZE  (sizeof (struct inotify_event))
#define BUF_LEN (1024 * (EVENT_SIZE + 16))

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "%s <watchpath> <destpath>\n", argv[0]);
        exit(-1);
    }

    int fd = inotify_init();

    if (fd < 0)
        perror ("inotify_init");

    int wd = inotify_add_watch(fd, argv[1], IN_CREATE);

    if (wd < 0)
        perror ("inotify_add_watch");

    char buf[BUF_LEN];
    fd_set rfds;
    int ret;

    /* zero-out the fd_set */
    FD_ZERO (&rfds);

    /*
     * add the inotify fd to the fd_set -- of course,
     * your application will probably want to add
     * other file descriptors here, too
     */
    FD_SET (fd, &rfds);

    while(1) {
        ret = select (fd+1, &rfds, NULL, NULL, NULL);

        if (FD_ISSET (fd, &rfds)) {
            int len, i = 0;

            len = read (fd, buf, BUF_LEN);

            while (i < len) {
                struct inotify_event *event;
                event = (struct inotify_event *) &buf[i];

                if (event->len) {
                    int len = strlen(event->name);

                    if (len > 7 && strncmp(&event->name[len-7], ".queued", 7) == 0) {
                        char stripfile[512], file[1024], hardlink[1024];
                        int namelen;

                        stripfile[0] = '\0';

                        strncpy(stripfile, event->name, len-7);
                        stripfile[len-7] = '\0';

                        snprintf(file, 1024, "%s/%s", argv[1], event->name);

                        if ((namelen = readlink(file, hardlink, 1024)) < 0)
                            perror("readlink");
                        else {
                            char cmd[1024];
                            char *filetodelete = strdup(file);
                            len = strlen(file);

                            strncpy(&file[len-7], ".busy\0", 6);

                            hardlink[namelen] = '\0';
			
		            	    snprintf(cmd, 1023, "cp %s %s", hardlink, file);
            			    cmd[1023] = '\0';

                            system(cmd);
	                        fprintf (stderr, "CP: source=%s name=%s\n", hardlink, file);
                            
                            fprintf (stderr, "DELETE: name=%s\n", filetodelete);
                            unlink(filetodelete);
                            free(filetodelete);

                            snprintf(cmd, 1023, "%s/%s", argv[2], stripfile);
            			    cmd[1023] = '\0';

	                        fprintf (stderr, "MV: source=%s name=%s\n", file, cmd);

                            rename(file, cmd);
                        }
                    }
                }

                i += EVENT_SIZE + event->len;
            }
        }

    }

}
