#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cherokee/cherokee.h>


get_all_configurations(cherokee_avl_r_t *avl_r, cherokee_buffer_t *user, cherokee_buffer_t *buffer) {
  int return_code;
  DIR *dir;
  struct dirent entry;
  struct dirent *result;
  char file[1024];

  snprintf(file, 1023, "/mnt/netapp/users/%s", user->buf);

  if ((dir = opendir(file)) == NULL)
    perror("opendir() error");
  else {
    unsigned int offset = strlen(file);
    file[offset] = '/';
    offset++;
    for (return_code = readdir_r(dir, &entry, &result);
         result != NULL && return_code == 0;
         return_code = readdir_r(dir, &entry, &result)) {
            struct stat statbuf;
            snprintf(&file[offset], 1023 - offset, "%s/index.xml", entry.d_name);

            if (stat(file, &statbuf) == 0) {
                cherokee_buffer_t domain = CHEROKEE_BUF_INIT;
                cherokee_buffer_t *host;
                cherokee_buffer_add_va (&domain, "%s_%s", user->buf, entry.d_name);

                ret_t status = cherokee_avl_r_get (avl_r, &domain, (void **) &host);
            
                cherokee_buffer_add_str (buffer, "  <domain");

                if (status == ret_ok) {
                    cherokee_buffer_add_str (buffer, " status=\"running\"");
                }

                cherokee_buffer_add_str (buffer, "><name>");
                cherokee_buffer_add_buffer(buffer, &domain);
                cherokee_buffer_add_str (buffer, "</name></domain>\n");

                cherokee_buffer_mrproper(&domain);
            } else {
                strcpy(&file[offset], "index.xml.deleted");
                if (stat(file, &statbuf) == 0)
                    cherokee_buffer_add_va (buffer, "  <domain status=\"deleted\"><name>%s_%s</name></domain>\n", user->buf, entry.d_name);
            }
    }
    if (return_code != 0)
      perror("readdir_r() error");
    closedir(dir);
  }
}

