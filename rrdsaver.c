#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <rrd.h>
#include <sys/stat.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

#include <libvirt/libvirt.h>

static AvahiSimplePoll *simple_poll = NULL;

static void resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {

    assert(r);

    /* Called whenever a service has been resolved successfully or timed out */

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            fprintf(stderr, "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
            avahi_service_resolver_free(r);
            break;

        case AVAHI_RESOLVER_FOUND: {
            char a[AVAHI_ADDRESS_STR_MAX], *t;
            
            fprintf(stderr, "Service '%s' of type '%s' in domain '%s':\n", name, type, domain);
            
            avahi_address_snprint(a, sizeof(a), address);
            t = avahi_string_list_to_string(txt);
            fprintf(stderr,
                    "\t%s:%u (%s)\n"
                    "\tTXT=%s\n"
                    "\tcookie is %u\n"
                    "\tis_local: %i\n"
                    "\tour_own: %i\n"
                    "\twide_area: %i\n"
                    "\tmulticast: %i\n"
                    "\tcached: %i\n",
                    host_name, port, a,
                    t,
                    avahi_string_list_get_service_cookie(txt),
                    !!(flags & AVAHI_LOOKUP_RESULT_LOCAL),
                    !!(flags & AVAHI_LOOKUP_RESULT_OUR_OWN),
                    !!(flags & AVAHI_LOOKUP_RESULT_WIDE_AREA),
                    !!(flags & AVAHI_LOOKUP_RESULT_MULTICAST),
                    !!(flags & AVAHI_LOOKUP_RESULT_CACHED));

            char *customer = strdup(name);
            char *vm = strchr(customer, '_');
            if (vm != NULL) {
                AvahiStringList *needle;
                *vm = '\0';
                vm++;

                if ((needle = avahi_string_list_find (txt, "interfaces")) != NULL) {
                    const char *delim = ",";
                    unsigned int no = 0;
                    char *winner, *tok;
//                    char *params[] = { "rxbytes", "rxpackets", "rxerrs", "rxdrop",
//                                       "txbytes", "txpackets", "txerrs", "txdrop" };
                    char *params[] = { "rxbytes", "rxpackets",
                                       "txbytes", "txpackets" };

                    avahi_string_list_get_pair (needle, NULL, &winner, NULL);
                    
                    tok = strtok (winner, delim);
                    while (tok != NULL) {
                        int i;
                        char rrdfile[1024];
                        char buf[1024];
                        struct stat statbuf;
                        snprintf(rrdfile, 1022, "/mnt/netapp/users/%s/%s/interface_eth%d.rrd", customer, vm, no);
                        rrdfile[1023] = '\0';

                        char *values[4];
                        
                        char *r_update[5] = {"update", rrdfile, "--template", "rxbytes:rxpackets:txbytes:txpackets", buf};

                        if (stat(rrdfile, &statbuf) != 0) {
                            char *r_create[14] = {"create", rrdfile, "-s 30",
                                                 "DS:rxbytes:DERIVE:1800:0:100000000000",
                                                 "DS:rxpackets:DERIVE:1800:0:100000000000",
                                                 "DS:txbytes:DERIVE:1800:0:100000000000",
                                                 "DS:txpackets:DERIVE:1800:0:100000000000",
                                                 "RRA:AVERAGE:0.5:1:1200",
                                                 "RRA:AVERAGE:0.5:10:1200",
                                                 "RRA:AVERAGE:0.5:120:2400" };
                            rrd_create(10, r_create);
                            if (rrd_test_error()) {
                                fprintf(stderr, "create: %s\n", rrd_get_error());
                                rrd_clear_error();
                            }
                        }

                        for (i = 0; i < 4; i++) {
                            AvahiStringList *newneedle;
                            char param[256];
                            snprintf(param, 253, "interface_%s_%s", tok, params[i]);
                            param[254] = '\0';

                            if ((newneedle = avahi_string_list_find (txt, param)) != NULL) {
                                avahi_string_list_get_pair (newneedle, NULL, &values[i], NULL);
                            } else {
                                values[i] = NULL;
                            }
                        }

                        snprintf(buf, 1022, "N:%s:%s:%s:%s",
                                            (values[0] == NULL?"U":values[0]),
                                            (values[1] == NULL?"U":values[1]),
                                            (values[6] == NULL?"U":values[2]),
                                            (values[7] == NULL?"U":values[3]));

                        printf("%s\n", buf);
                        rrd_update(5, r_update);

                        for (i = 0; i < 4; i++) {
                            free(values[i]);
                        }

                        tok = strtok (NULL, delim);
                    }

                    free(winner);
                    no++;
                }
               
                if ((needle = avahi_string_list_find (txt, "cpuTime")) != NULL) {
                    char rrdfile[1024];
                    snprintf(rrdfile, 1023, "/mnt/netapp/users/%s/%s/cpuTime.rrd", customer, vm);
                    rrdfile[1023] = '\0';                
                    char *winner;
                    avahi_string_list_get_pair (needle, NULL, &winner, NULL);
                    if (winner != NULL) {
                        struct stat statbuf;
                        char buf[128];
                        char *r_update[3] = {"update", rrdfile, buf};

                        if (stat(rrdfile, &statbuf) != 0) {
                            char *r_create[7] = {"create", rrdfile, "-s 30", "DS:cpuTime:DERIVE:1800:0:100000000000", "RRA:AVERAGE:0.5:1:1200", "RRA:AVERAGE:0.5:10:1200", "RRA:AVERAGE:0.5:120:2400" };
                            rrd_create(7, r_create);
                            if (rrd_test_error()) {
                                fprintf(stderr, "create: %s\n", rrd_get_error());
                                rrd_clear_error();
                            }
                        }

                        snprintf(buf, 127, "N:%s", winner);
                        buf[127] = '\0';
                        avahi_free(winner);
                        rrd_update(3, r_update);

                        if (rrd_test_error()) {
                            fprintf(stderr, "%s\n", rrd_get_error());
                            rrd_clear_error();
                        }
                    }
                }
            }
            free(customer);

            avahi_free(t);
        }
    }

}

static void browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {
    
    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */

    switch (event) {
        case AVAHI_BROWSER_FAILURE:
            fprintf(stderr, "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_service_browser_free(b);
            return;

        case AVAHI_BROWSER_NEW: {
            AvahiClient *c = avahi_service_browser_get_client(b);
            fprintf(stderr, "(Browser) NEW: service '%s' of type '%s' in domain '%s'\n", name, type, domain);

            /* We ignore the returned resolver object. In the callback
               function we free it. If the server is terminated before
               the callback function is called the server will free
               the resolver for us. */
            
            if (!(avahi_service_resolver_new(c, interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, 0, resolve_callback, c)))
                fprintf(stderr, "Failed to resolve service '%s': %s\n", name, avahi_strerror(avahi_client_errno(c)));
            
            break;
        }

        case AVAHI_BROWSER_REMOVE:
            fprintf(stderr, "(Browser) REMOVE: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
            break;

        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
            fprintf(stderr, "(Browser) %s\n", event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
            break;
    }
}

static void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata) {
//    assert(c);

    /* Called whenever the client or server state changes */

    switch (state) {
        case AVAHI_CLIENT_S_RUNNING: {
            /* The server has startup successfully and registered its host
             * name on the network, so it's time to create our services */
            AvahiServiceBrowser *sb = NULL;

            /* Create the service browser */
            if (!(sb = avahi_service_browser_new(c, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, "_domu._tcp", NULL, 0, browse_callback, NULL))) {
                fprintf(stderr, "Failed to create service browser: %s\n", avahi_strerror(avahi_client_errno(c)));
                avahi_simple_poll_quit(simple_poll);
            }
            break;
        }

        case AVAHI_CLIENT_FAILURE: {
            int error;
            AvahiClient *client = NULL;

            if (c)
                avahi_client_free(c);

            /* Allocate a new client */
            client = avahi_client_new(avahi_simple_poll_get(simple_poll), AVAHI_CLIENT_NO_FAIL, client_callback, NULL, &error);

            /* Check wether creating the client object succeeded */
            if (!client) {
                fprintf(stderr, "Failed to create client: %s\n", avahi_strerror(error));
                avahi_simple_poll_quit(simple_poll);
            }

            break;
        }

        case AVAHI_CLIENT_S_COLLISION:
            /* Let's drop our registered services. When the server is back
             * in AVAHI_SERVER_RUNNING state we will register them
             * again with the new host name. */

        case AVAHI_CLIENT_S_REGISTERING:
            /* The server records are now being established. This
             * might be caused by a host name change. We need to wait
             * for our own records to register until the host name is
             * properly esatblished. */
            break;

        case AVAHI_CLIENT_CONNECTING:
            ;
    }
}


int main(AVAHI_GCC_UNUSED int argc, AVAHI_GCC_UNUSED char*argv[]) {
    int ret = 1;

    /* Allocate main loop object */
    if (!(simple_poll = avahi_simple_poll_new())) {
        fprintf(stderr, "Failed to create simple poll object.\n");
        goto fail;
    }

    client_callback(NULL, AVAHI_CLIENT_FAILURE, NULL);

    /* Run the main loop */
    avahi_simple_poll_loop(simple_poll);

    ret = 0;

fail:
    if (simple_poll)
        avahi_simple_poll_free(simple_poll);

    return ret;
}
