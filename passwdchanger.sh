#!/bin/bash

# Stefan de Konink <dekonink@kinkrsoftware.nl>
# Licensed under the GPL v3 or in your opinion any later version
# 
# Usage passwdchanger <imagefile> <passwd>

DIR=/tmp/`basename $1`
mkdir $DIR &&
mount -o loop $1 $DIR &&
chroot $DIR /usr/sbin/chpasswd <<-EOT
root:$2
EOT
umount $DIR &&
rmdir $DIR
