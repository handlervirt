#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/file.h>


int main() {
    FILE *fd;
    char mac[13];
    unsigned long int counter = 0;

    int locker = open("/mnt/netapp/maccounter", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    flock(locker, LOCK_EX);

    if ((fd = fdopen(locker, "w+")) != NULL) {
        if (fread(mac, 12, sizeof(char), fd) == 1) {
            mac[12] = '\0';
            counter = strtoul(mac, NULL, 16);
            counter++;
        }
        snprintf(mac, 13, "%012lX\n", counter);
        rewind(fd);
        fwrite(mac, 12, sizeof(char), fd);
        fclose(fd);

        char pretty[18];
        pretty[2] = ':'; pretty[5] = ':'; pretty[8] = ':'; pretty[11] = ':'; pretty[14] = ':'; pretty[17] = '\0';
        strncpy(&pretty[0], &mac[0], 2);
        strncpy(&pretty[3], &mac[2], 2);
        strncpy(&pretty[6], &mac[4], 2);
        strncpy(&pretty[9], &mac[6], 2);
        strncpy(&pretty[12], &mac[8], 2);
        strncpy(&pretty[15], &mac[10], 2);

        // dhcp-host=00:1E:C9:B3:B3:85,xen001.xen,172.16.103.1,24h
        printf("dhcp-host=%s,%s.xen,...,24h\n", pretty, "test");

//        fd = fopen("/mnt/netapp/dnsmasq.conf", "a");
//        fprintf(fd, "dhcp-host=%s,%s,", 10, sizeof(char)
//        fclose(fd);
    }

    flock(locker, LOCK_UN);
    close(locker);

    return 0;
}
