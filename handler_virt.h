/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CHEROKEE_HANDLER_VIRT_H
#define CHEROKEE_HANDLER_VIRT_H

/*
#include "common.h"
#include "buffer.h"
#include "handler.h"
#include "connection.h"
#include "plugin_loader.h"
*/

#include <libvirt/libvirt.h>
#include <cherokee/cherokee.h>
#include "handler_clusterstats.h"

/* Data types
 */
typedef struct {
	cherokee_handler_clusterstats_t  base;

    /* Configuration parameters */
    cherokee_boolean_t   authenticate;
    cherokee_boolean_t   read_only;
    cherokee_buffer_t    xsl;
    cherokee_buffer_t    virt;
} cherokee_handler_virt_props_t;

typedef struct {
    cherokee_handler_t  base;
    cherokee_buffer_t   buffer;
    cherokee_buffer_t   user;
    cherokee_buffer_t   vm;
    enum {
        not_authenticated = -2,
        not_implemented = -1,
        nothing = 0,
        domainAttachDevice,
        domainAttachDevice_args,
        domainDetachDevice,
        domainGetID,
        domainGetMaxMemory,
        domainGetMaxVcpus,
        domainGetName,
        domainGetOSType,
        domainGetSchedulerParameters,
        domainGetSchedulerType,
        domainGetUUID,
        domainGetUUIDString,
        domainGetVcpus,
        domainCreate,
        domainDestroy,
        domainReboot,
        domainRestore,
        domainMigrate_args,
        domainSave,
        domainShutdown,
        domainInterfaceStats,
        domainUndefine,
        storageVolCreateXML,
        storageVolCreateXML_args,
        storageVolCloneXML_args,
        storageVolCloneStatus_args,
        storageVolDelete_args,
        storageVolSetPassword_args,
        graph,
        graphLoad_args,
        graphInterface_args,
        xml,
        domainDefineXML_args,
        domainDefineXML,
        domainGetXMLDesc,
        domainGetXMLSnapshots,
        storageVolGetXMLDesc_args,
        showall,
        showuservms
    } action;
} cherokee_handler_virt_t;

#define HDL_VIRT(x)       ((cherokee_handler_virt_t *)(x))
#define PROP_VIRT(x)      ((cherokee_handler_virt_props_t *)(x))
#define HDL_VIRT_PROPS(x) (PROP_VIRT(MODULE(x)->props))

/* Library init function
 */
void  PLUGIN_INIT_NAME(virt)      (cherokee_plugin_loader_t *loader);
ret_t cherokee_handler_virt_new   (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props);

/* virtual methods implementation
 */
ret_t cherokee_handler_virt_init        (cherokee_handler_virt_t *hdl);

static ret_t virt_build_page (cherokee_handler_virt_t *hdl);

static virStorageVolPtr virt_get_vol_by_args(cherokee_handler_virt_t *hdl, virConnectPtr virConn, unsigned short int prefix);

static ret_t load_xml(cherokee_handler_virt_t *hdl, cherokee_buffer_t *xmlDesc);
static ret_t save_xml(cherokee_handler_virt_t *hdl, virDomainPtr result, cherokee_buffer_t *buf);

#endif /* CHEROKEE_HANDLER_VIRT_H */
