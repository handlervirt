/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CHEROKEE_HANDLER_EXAMPLE_H
#define CHEROKEE_HANDLER_EXAMPLE_H

/*
#include "common.h"
#include "buffer.h"
#include "handler.h"
#include "connection.h"
#include "plugin_loader.h"
*/

#include <cherokee/cherokee.h>

/* Data types
 */
typedef struct {
	cherokee_module_props_t  base;

    /* Configuration parameters */
	cherokee_boolean_t       example_config;
} cherokee_handler_example_props_t;

typedef struct {
    /* Shared structures */
	cherokee_handler_t       handler;

    /* A buffer is your output 'to be' */
	cherokee_buffer_t        buffer;

    enum {
        send_page,
        send_yourmother /* not advised but possible */
    } action; /* this could implement a state machine */
} cherokee_handler_example_t;

#define HDL_EXAMPLE(x)       ((cherokee_handler_example_t *)(x))
#define PROP_EXAMPLE(x)      ((cherokee_handler_example_props_t *)(x))
#define HDL_EXAMPLE_PROPS(x) (PROP_EXAMPLE(MODULE(x)->props))


/* Library init function
 */
void  PLUGIN_INIT_NAME(example)      (cherokee_plugin_loader_t *loader);
ret_t cherokee_handler_example_new   (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props);

/* virtual methods implementation
 */
ret_t cherokee_handler_example_init        (cherokee_handler_example_t *hdl);
ret_t cherokee_handler_example_free        (cherokee_handler_example_t *hdl);
ret_t cherokee_handler_example_step        (cherokee_handler_example_t *hdl, cherokee_buffer_t *buffer);
ret_t cherokee_handler_example_add_headers (cherokee_handler_example_t *hdl, cherokee_buffer_t *buffer);

#endif /* CHEROKEE_HANDLER_EXAMPLE_H */
