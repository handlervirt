#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>

typedef struct line LINE;

struct line {
    unsigned long int mac;
    unsigned long int ip;
    char *fqdn;
    LINE *next;
};

LINE *first = NULL;
LINE *last  = NULL;
LINE *array   = NULL;

int listlen = 0;

/* Removes an entry from our linked list */

static int remove_entry(unsigned long int mac) {
    LINE *step = first;
    LINE *prev = NULL;
    while (step) {
        LINE *next = step->next;

        if (step->mac == mac) {
            if (step->fqdn)
                free(step->fqdn);
            
            if (prev)
                prev->next = next;
            free(step);
        }

        prev = step;
        step = next;
    }

    return 0;
}

/* Adds an entry to the linked list */

static int add_entry(unsigned long int mac, unsigned long int ip, char *fqdn) {
    LINE *new = malloc(sizeof(LINE));
    new->mac = mac;
    new->ip  = ip;
    new->fqdn = strdup(fqdn);
    new->next = NULL;

    if (!first)
        first = new;
    else
        last->next = new;
    
    last = new;

    return 0;
}

/* Converts a MA:CA:DD:RE:SS:00 to an unsigned long int */

static unsigned long int macstrtoul(char *mac) {
    char macstring[4];
    macstring[2] = '\0';
    macstring[3] = '\0';

    unsigned long int newmac = 0;
    strncpy(macstring, &mac[9], 2);
    newmac += (strtoul(macstring, NULL, 16) * (unsigned long int)(256 * 256));
    strncpy(macstring, &mac[12], 2);
    newmac += (strtoul(macstring, NULL, 16) * (unsigned long int)(256));
    strncpy(macstring, &mac[15], 2);
    newmac += (strtoul(macstring, NULL, 16));

    return newmac;
}

/* Parses a line from the file and adds it to the linked list */

static int add_line(char *line) {
    if (strncmp(line, "dhcp-host=", 10) == 0 &&
                line[12] == ':' && line[15] == ':' && line[18] == ':' && line[21] == ':' && line[24] == ':' &&
                line[27] == ',') {
        char *ip, *nextip;
        char macstring[4];
        LINE *new = malloc(sizeof(LINE));

        if (new == NULL) return -1;

        listlen++;
        macstring[2] = '\0';
        macstring[3] = '\0';

        new->mac = 0;
        new->ip  = 0;
        new->next= NULL;

        new->mac = macstrtoul(&line[10]);

        ip = strchr(&line[28], ',');
        new->fqdn = strndup(&line[28], ip - &line[28]);
        ip++;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256 * 256 * 256));
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256 * 256));
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256));
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, ',')) - ip);
        new->ip += (strtoul(macstring, NULL, 10));

        if (!first)
            first = new;
        else
            last->next = new;

        last = new;

        return 0;
    }

    return -1;
}

/* Prints out our mac range */

static char * print_mac(unsigned long int mac) {
    char *macstring = (char *) malloc(sizeof(char*) * 18);
    strcpy(macstring, "00:16:3E:");
    snprintf(&macstring[9], 8, "%06lX", mac);
    strncpy(&macstring[15], &macstring[13], 2);
    macstring[13] = macstring[12];
    macstring[12] = macstring[11];
    macstring[11] = ':';
    macstring[14] = ':';
    macstring[17] = '\0';
    return macstring;
}

/* And prints out an ip address */

static char * print_ip(unsigned long int ipaddress) {
    char *dest = (char *) malloc(sizeof(char*) * 16);
    int a, b, c, d;
    ldiv_t ip = ldiv(ipaddress, (256*256*256));
    a = ip.quot;
    ip = ldiv(ip.rem, (256*256));
    b = ip.quot;
    ip = ldiv(ip.rem, (256));
    c = ip.quot;
    d = ip.rem;

    snprintf(dest, 16, "%d.%d.%d.%d", a, b, c, d);
    return dest;
}

/* Print out the dnsmasq format and clean up the linked list */

static int print_forget(char *path) {
    FILE *fd = fopen(path, "w+");

    LINE *step = first;
    while (step) {
        LINE *this = step;
        char *pretty_mac = print_mac(this->mac);
        char *pretty_ip  = print_ip(this->ip);
        
        if (fd)
            fprintf(fd, "dhcp-host=%s,%s,%s,24h\n", pretty_mac, this->fqdn, pretty_ip);

        free(pretty_mac);
        free(pretty_ip);

        free(this->fqdn);

        step = this->next;
        free(this); 
    }

    if (fd)
        fclose(fd);

    return 0;
}

/* Convert the linked list to a more 'defacto' array format */

static int ll_array() {
    int i = 0;
    array = malloc(sizeof(LINE) * listlen);

    LINE *step = first;
    while (step) {
        array[i].mac  = step->mac;
        array[i].ip  = step->ip;
        array[i].fqdn = step->fqdn;
        step = step->next;
        i++;
    }

    return 0;
}

/* Compare functions */

static int bymac(const void *a, const void *b) {
    LINE *temp1 = (LINE *) a;
    LINE *temp2 = (LINE *) b;

    return temp1->mac - temp2->mac;
}

static int byip(const void *a, const void *b) {
    LINE *temp1 = (LINE *) a;
    LINE *temp2 = (LINE *) b;

    return temp1->ip - temp2->ip;
}

/* Find the first 'free' mac */

static unsigned long int nextmac() {
    int i;
    for (i = 0; i < (listlen - 1); i++) {
        if (array[i+1].mac != 0 && array[i+1].mac != array[i].mac + 1) return array[i].mac + 1;
    }
    return array[listlen - 1].mac + 1;
}

/* Find the first 'free' ip */
static unsigned long int nextip() {
    int i;
    for (i = 0; i < (listlen - 1); i++) {
        if (array[i+1].ip != array[i].ip + 1) return array[i].ip + 1;
    }
    return array[listlen - 1].ip + 1;
}

/* Opens the cabinet config */

static char * loader(char *cabinet) {
    FILE *fd;
    char *pad = malloc(sizeof(char) * 128);

    listlen = 0;
    first = NULL;
    last  = NULL;
    array   = NULL;

    if (pad == NULL) return NULL;

    snprintf(pad, 128, "/mnt/netapp/ipservices/%s.conf", cabinet);

    if ((fd = fopen(pad, "r")) != NULL) {
        char line[128];
        while (!feof(fd)) {
            if (fgets(line, 128, fd) > 0)
                add_line(line);
        }
        fclose(fd);
    }

    return pad;
}

/* Exported functions */
/* TODO: implement better locking */

int removeOldMac(char *cabinet, char *mac) {
    char *pad = loader(cabinet);
    
    if (pad == NULL)
        return -1;

    unsigned long int old_mac = macstrtoul(mac);
    remove_entry(old_mac);
    print_forget(pad);
    free(pad);
    return 0;
}


int getNewMac(char *cabinet, char *fqdn, char **mac, char **ip) {
    unsigned long int new_mac, new_ip;
    char *pad = loader(cabinet);

    if (pad == NULL)
        return -1;
        
    if (listlen == 0) {
        new_mac = 0;
        new_ip = 0;
    } else {
        ll_array();

        qsort(array, listlen, sizeof(LINE), bymac);
        new_mac = nextmac();

        qsort(array, listlen, sizeof(LINE), byip);
        new_ip = nextip();
    }

    if (mac)
        *mac = print_mac(new_mac);
    
    if (ip)
        *ip = print_ip(new_ip);

    add_entry(new_mac, new_ip, fqdn);

    free(array);

    print_forget(pad);
    
    free(pad);

    return 0;
}

/* Good for unittesting */

#ifdef WITH_MAIN
int main(int argc, char *argv[]) {
    if (argc == 4) {
        removeOldMac(argv[1], argv[2]);
    } else if (argc == 3) {
        getNewMac(argv[1], argv[2], NULL, NULL);
    }
    return 0;
}
#endif
