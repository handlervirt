#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>

#include <libvirt/libvirt.h>


virConnectPtr conn = NULL; /* the hypervisor connection */

int main(int argc, char*argv[]) {
    int error;
    int ret = 1;

    /* NULL means connect to local Xen hypervisor */
    conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL) {
        fprintf(stderr, "Failed to connect to hypervisor\n");
    }

    if (argc == 2) {
        unsigned long long required = strtoul(argv[1], (char **) NULL, 10);
        unsigned long long free = virNodeGetFreeMemory(conn);
        if (errno != ERANGE)
            printf("%f\n", (float)required / (float)free);
    }

        printf("%lu\n", virNodeGetFreeMemory(conn));

    virDomainPtr domu = virDomainLookupByName(conn, "klant1_testdomein");
    printf("%lu\n", virDomainGetMaxMemory(domu));
    virDomainFree(domu);

    virStoragePoolPtr pool = virStoragePoolLookupByName(conn, "netapp-nfs");
    //virStorageVolPtr volume = virStorageVolLookupByName(pool, "klant1_test.qcow");
    virStorageVolPtr volume = virStorageVolLookupByName(pool, "distro/gentoo.qcow");

    if (volume == NULL) 
        printf("die bestaat niet!\n");

    printf("%\n\n%s",  virStorageVolGetKey(volume), virStorageVolGetXMLDesc(volume, 0));

    virStoragePoolFree(pool);

    virConnectClose(conn);

    return ret;
}
