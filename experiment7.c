#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef struct line LINE;

struct line {
    unsigned long int mac;
    unsigned long int ip;
    char *fqdn;
    LINE *next;
};

int add(LINE *first, LINE *last, unsigned long int mac, unsigned long int ip, char *fqdn) {
    LINE *new = malloc(sizeof(LINE));
    new->mac = mac;
    new->ip  = ip;
    new->fqdn = strdup(fqdn);
    new->next = NULL;

    if (!first)
        first = new;
    else
        last->next = new;
    
    last = new;

    return 0;
}

int add_line(LINE *first, LINE *last, char *line) {
    if (strncmp(line, "dhcp-host=", 10) == 0 &&
                line[12] == ':' && line[15] == ':' && line[18] == ':' && line[21] == ':' && line[24] == ':' &&
                line[27] == ',') {
        char *ip, *nextip;
        char macstring[4];
        LINE *new = malloc(sizeof(LINE));

        if (new == NULL) return -1;

        macstring[2] = '\0';
        macstring[3] = '\0';

        new->mac = 0;
        new->ip  = 0;
        new->next= NULL;

        strncpy(macstring, &line[19], 2);
        new->mac += (strtoul(macstring, NULL, 16) * (unsigned long int)(256 * 256));
        strncpy(macstring, &line[22], 2);
        new->mac += (strtoul(macstring, NULL, 16) * (unsigned long int)(256));
        strncpy(macstring, &line[25], 2);
        new->mac += (strtoul(macstring, NULL, 16));

        ip = strchr(&line[28], ',');
        new->fqdn = strndup(&line[28], ip - &line[28]);
        printf("%s\n", new->fqdn);
        ip++;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256 * 256 * 256));
//        printf("%s %lu\n",  macstring, new->ip);
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256 * 256));
//        printf("%s %lu\n",  macstring, (strtoul(macstring, NULL, 10) * (unsigned long int)(256 * 256)));
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, '.')) - ip);
        new->ip += (strtoul(macstring, NULL, 10) * (unsigned long int)(256));
//        printf("%s %lu\n",  macstring, strtoul(macstring, NULL, 10) * (unsigned long int)(256));
        ip = nextip + 1;

        macstring[1] = '\0'; macstring[2] = '\0';
        strncpy(macstring, ip, (nextip = strchr(ip, ',')) - ip);
        new->ip += (strtoul(macstring, NULL, 10));
//        printf("%s %lu\n",  macstring, strtoul(macstring, NULL, 10));

//        printf("%06lX %s %lu %lu\n", new->mac, new->fqdn, new->mac, new->ip);
//            dhcp-host=00:1E:C9:B3:B3:85,xen001.xen,172.16.103.1,24h
//

        if (!first)
            first = new;
        else
            last->next = new;

        last = new;

        return 0;
    }

    return -1;
}

char * print_mac(unsigned long int mac) {
    char *macstring = (char *) malloc(sizeof(char*) * 18);
    strcpy(macstring, "00:16:3E:");
    snprintf(&macstring[9], 8, "%06lX", mac);
    strncpy(&macstring[15], &macstring[13], 2);
    macstring[13] = macstring[12];
    macstring[12] = macstring[11];
    macstring[11] = ':';
    macstring[14] = ':';
    macstring[17] = '\0';
    return macstring;
}

char * print_ip(unsigned long int ipaddress) {
    char *dest = (char *) malloc(sizeof(char*) * 16);
    int a, b, c, d;
    ldiv_t ip = ldiv(ipaddress, (256*256*256));
    a = ip.quot;
    ip = ldiv(ip.rem, (256*256));
    b = ip.quot;
    ip = ldiv(ip.rem, (256));
    c = ip.quot;
    d = ip.rem;

    snprintf(dest, 16, "%d.%d.%d.%d", a, b, c, d);
    return dest;
}

int print_forget(LINE *first, char *path) {
    FILE *fd = fopen(path, "w");

    LINE *step = first;
    while (step) {
        LINE *this = step;
        char *pretty_mac = print_mac(this->mac);
        char *pretty_ip  = print_ip(this->ip);
        
        if (fd)
            fprintf(fd, "dhcp-host=%s,%s,%s,24h\n", pretty_mac, this->fqdn, pretty_ip);

        free(pretty_mac);
        free(pretty_ip);

        if (this->fqdn != NULL)
            free(this->fqdn);

        step = this->next;
        free(this); 
    }

    if (fd) {
        fclose(fd);
        return 0;
    }

    return -1;
}

static int bymac(const void *a, const void *b) {
    LINE *temp1 = (LINE *) a;
    LINE *temp2 = (LINE *) b;

    return temp1->mac - temp2->mac;
}

static int byip(const void *a, const void *b) {
    LINE *temp1 = (LINE *) a;
    LINE *temp2 = (LINE *) b;

    return temp1->ip - temp2->ip;
}

static unsigned long int nextmac(LINE *array, int listlen) {
    int i;
    for (i = 0; i < (listlen - 1); i++) {
        if (array[i+1].mac != 0 && array[i+1].mac != array[i].mac + 1) return array[i].mac + 1;
    }
    return array[listlen - 1].mac + 1;
}

static unsigned long int nextip(LINE *array, int listlen) {
    int i;
    for (i = 0; i < (listlen - 1); i++) {
        if (array[i+1].ip != array[i].ip + 1) return array[i].ip + 1;
    }
    return array[listlen - 1].ip + 1;
}

int getNewMac(char *cabinet, char *fqdn, char **mac, char **ip) {
    if (fqdn == NULL)
        return -1;

    char *pad = malloc(sizeof(char) * 128);

    if (pad == NULL) return -1;

    LINE *first = NULL, *last = NULL, *array = NULL;
    snprintf(pad, 128, "/mnt/netapp/ipservices/%s.conf", cabinet);

    FILE *fd;

    int listlen = 0;

    if ((fd = fopen(pad, "r")) != NULL) {
        char line[128];
        while (!feof(fd)) {
            if (fgets(line, 128, fd) > 0)
                if (add_line(first, last, line) == 0)
                    listlen++;
        }
        fclose(fd);
    }

    if (listlen > 0) {
        array = malloc(sizeof(LINE) * listlen);
        if (array == NULL) {
            // TODO: cleanup
            free(pad);
            return -1;
        } else {
            int i = 0;
            LINE *step = first;
            while (step) {
                printf("i = %d\n", i);
                array[i].mac  = step->mac;
                array[i].ip  = step->ip;
                array[i].fqdn = step->fqdn;
                step = step->next;
                i++;
            }
        }
        unsigned long int new_mac, new_ip;
        
        qsort(array, listlen, sizeof(LINE), bymac);
        new_mac = nextmac(array, listlen);
        if (mac)
            *mac = print_mac(new_mac);

        qsort(array, listlen, sizeof(LINE), byip);
        new_ip = nextip(array, listlen);

        if (ip)
            *ip = print_ip(new_ip);

        add(first, last, new_mac, new_ip, fqdn);

        free(array);

        print_forget(first, pad);

        free(pad);
        return 0;
    }
    free(pad);

    return -1;
}

#ifdef WITH_MAIN
int main(int argc, char *argv[]) {
    if (argc == 3) {
        getNewMac(argv[1], argv[2], NULL, NULL);
    }
    return 0;
}
#endif
