#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

#include <libvirt/libvirt.h>

#define POLL 5

struct list_el {
    unsigned int domainid;
    AvahiEntryGroup *group;
    unsigned short keep;
};

typedef struct list_el domu;

domu * domus = NULL; /* list of domains */
unsigned int domu_count = 0;
static AvahiSimplePoll *simple_poll = NULL;

static AvahiStringList * domainProperties(virDomainPtr thisDomain) {
    AvahiStringList *new = avahi_string_list_new(NULL);
    virDomainInfo info;
    virDomainInterfaceStatsStruct stats;
    virDomainGetInfo(thisDomain, &info);
    new = avahi_string_list_add_printf(new, "maxMem=%lu", info.maxMem);
    new = avahi_string_list_add_printf(new, "memory=%lu", info.memory);
    new = avahi_string_list_add_printf(new, "cpuTime=%llu", info.cpuTime);
    
    /* TODO: multiple network interfaces :( */
    char vifname[16];
    snprintf(vifname, 14, "vif%d.0", virDomainGetID(thisDomain));
    vifname[15] = '\0';
    unsigned int no = 0;
    
    virDomainInterfaceStats(thisDomain, vifname, &stats, sizeof(stats));

    new = avahi_string_list_add_printf(new, "interfaces=eth%d", no);
    new = avahi_string_list_add_printf(new, "interface_eth%d_rxbytes=%lld",   no, stats.rx_bytes);
    new = avahi_string_list_add_printf(new, "interface_eth%d_rxpackets=%lld", no, stats.rx_packets);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_rxerrs=%lld",    no, stats.rx_errs);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_rxdrop=%lld",    no, stats.rx_drop);
    new = avahi_string_list_add_printf(new, "interface_eth%d_txbytes=%lld",   no, stats.tx_bytes);
    new = avahi_string_list_add_printf(new, "interface_eth%d_txpackets=%lld", no, stats.tx_packets);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_txerrs=%lld",    no, stats.tx_errs);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_txdrop=%lld",    no, stats.tx_drop);

    return new;
}

static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, AVAHI_GCC_UNUSED void *userdata) {
    /* Called whenever the entry group state changes */

    switch (state) {
        case AVAHI_ENTRY_GROUP_ESTABLISHED :
            /* The entry group has been established successfully */
            //fprintf(stderr, "Service '%s' successfully established.\n", name);
            break;

        case AVAHI_ENTRY_GROUP_COLLISION : {
//            char *n;

            /* A service name collision with a remote service
             * happened. Let's pick a new name */
            //n = avahi_alternative_service_name(name);
            //avahi_free(name);
            //name = n;

//            fprintf(stderr, "Service name collision, renaming service to '%s'\n", name);

            /* And recreate the services */
            //create_services(avahi_entry_group_get_client(g));
            break;
        }

        case AVAHI_ENTRY_GROUP_FAILURE :

            fprintf(stderr, "Entry group failure: %s\n", avahi_strerror(avahi_client_errno(avahi_entry_group_get_client(g))));

            /* Some kind of failure happened while we were registering our services */
            avahi_simple_poll_quit(simple_poll);
            break;

        case AVAHI_ENTRY_GROUP_UNCOMMITED:
        case AVAHI_ENTRY_GROUP_REGISTERING:
            ;
    }
}

void create_services(AvahiClient *c) {
    virConnectPtr conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL)
        return;
    int maxid = virConnectNumOfDomains(conn);
    if (maxid > 1) { /* ignore dom0 */
        int *ids = (int *) malloc(sizeof(int) * maxid);
        if ((maxid = virConnectListDomains(conn, &ids[0], maxid)) < 0) {
            // error
        } else {
            int i;
            unsigned int domu_count_new = (maxid - 1);
            domu * domus_old = domus;
            domus = (domu *) malloc(sizeof(domu) * domu_count_new);
            const char *type = "_domu._tcp";
            for (i = 0; i < domu_count_new; i++) {
                int j;
                domus[i].group = NULL;
                domus[i].domainid = ids[i+1];
                domus[i].keep = 0;

                //fprintf(stderr, "Looking for %d\n", domus[i].domainid);

                for (j = 0; j < domu_count; j++) {
                    //fprintf(stderr, "--> %d\n", domus_old[j].domainid);
                    if (ids[i+1] == domus_old[j].domainid) {
                        //fprintf(stderr, "got it\n");
                        virDomainPtr thisDomain = virDomainLookupByID(conn, domus[i].domainid);
                        domus[i].group = domus_old[j].group;
                        if (thisDomain) {
                            const char *name = virDomainGetName(thisDomain);
                            if (name) {
                                AvahiStringList *domainProps;
                                domainProps = domainProperties(thisDomain);
                                domus_old[j].keep = 1;
                                avahi_entry_group_update_service_txt_strlst(domus[i].group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, name, type, NULL, domainProps);
                                avahi_string_list_free(domainProps);
                            } else {
                                // domus_old[j].keep = 0;
                            }
                        } else {
                            // domus_old[j].keep = 0;
                        }
                        virDomainFree(thisDomain);
                    }
                }
                if (i > domu_count || domus[i].group == NULL) {
                    const char *name;
                    AvahiStringList *domainProps;
                    virDomainPtr thisDomain = virDomainLookupByID(conn, ids[i+1]);
                    domus[i].group = avahi_entry_group_new(c, entry_group_callback, NULL);
                    domainProps = domainProperties(thisDomain);
                    name = virDomainGetName(thisDomain);
                    
                    if (name) {
                        avahi_entry_group_add_service_strlst(domus[i].group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, name, type, NULL, NULL, 651, domainProps);
                        avahi_entry_group_commit(domus[i].group);
                    } else {
                        domus[i].keep = 0;
                    }
                        
                    avahi_string_list_free(domainProps);
                    virDomainFree(thisDomain);

                }
            }

            for (i = 0; i < domu_count; i++) {
                // fprintf(stderr, "id: %d, keep: %d\n", domus_old[i].domainid, domus_old[i].keep);
                if (domus_old[i].keep == 0 && domus_old[i].group != NULL) {
                    avahi_entry_group_free(domus_old[i].group);
                }
            }

            free(domus_old);
            domu_count = domu_count_new;
        }
        free(ids);
    }
    virConnectClose(conn);
    //fprintf(stderr, "exit: %s\n", __func__);
}

static void modify_callback(AvahiTimeout *e, void *userdata) {
    struct timeval tv;
    AvahiClient *client = userdata;

    /* If the server is currently running, we need to remove our
     * service and create it anew */
    if (avahi_client_get_state(client) == AVAHI_CLIENT_S_RUNNING) {
        /* And create them again with the new name */
        create_services(client);

        avahi_simple_poll_get(simple_poll)->timeout_update(e, avahi_elapse_time(&tv, 1000*POLL, 0));
    }
}

static void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata) {

    /* Called whenever the client or server state changes */

    switch (state) {
        case AVAHI_CLIENT_S_RUNNING: {
            /* The server has startup successfully and registered its host
             * name on the network, so it's time to create our services */
            
            struct timeval tv;
            /* After 10s do some weird modification to the service */
            avahi_simple_poll_get(simple_poll)->timeout_new(
                avahi_simple_poll_get(simple_poll),
                avahi_elapse_time(&tv, 1000*POLL, 0),
                modify_callback,
                c);
            
            break;
        }

        case AVAHI_CLIENT_FAILURE: {
            int error;
            AvahiClient *client = NULL;

            if (c)
                avahi_client_free(c);

            /* Allocate a new client */
            client = avahi_client_new(avahi_simple_poll_get(simple_poll), AVAHI_CLIENT_NO_FAIL, client_callback, NULL, &error);

            /* Check wether creating the client object succeeded */
            if (!client) {
                /* If you look at the above argument, this should NEVER
                   happen */
                fprintf(stderr, "Failed to create client: %s\n", avahi_strerror(error));
                avahi_simple_poll_quit(simple_poll);
            }

           break;
        }

        case AVAHI_CLIENT_S_COLLISION:

            /* Let's drop our registered services. When the server is back
             * in AVAHI_SERVER_RUNNING state we will register them
             * again with the new host name. */

        case AVAHI_CLIENT_S_REGISTERING:

            /* The server records are now being established. This
             * might be caused by a host name change. We need to wait
             * for our own records to register until the host name is
             * properly esatblished. */

            break;

        case AVAHI_CLIENT_CONNECTING:
            ;
    }
}



int main(AVAHI_GCC_UNUSED int argc, AVAHI_GCC_UNUSED char*argv[]) {
    int ret = 1;
    /* Allocate main loop object */
    if (!(simple_poll = avahi_simple_poll_new())) {
        fprintf(stderr, "Failed to create simple poll object.\n");
        goto fail;
    }

    client_callback(NULL, AVAHI_CLIENT_FAILURE, NULL);

    /* Run the main loop */
    avahi_simple_poll_loop(simple_poll);

    ret = 0;

fail:
    /* Cleanup things */
    if (simple_poll)
        avahi_simple_poll_quit(simple_poll);

    if (simple_poll)
        avahi_simple_poll_free(simple_poll);

    if (domus)
        free(domus);

    return ret;
}

