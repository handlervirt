/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

// #define LINUX_CMDLINE "root=/dev/xvda ro ip=%s:1.2.3.4:%s:%s::eth0:off"

#define VIRT_INTERFACE_XML \
"    <interface type='bridge'>" \
"      <source bridge='xenbr0'/>" \
"      <mac address='%s'/>" \
"      <ip address='%s' />" \
"      <script path='vif-bridge'/>" \
"    </interface>"

//"      <target dev='vif1.0'/>" \

#define VIRT_DISK_XML \
"    <disk type='file' device='disk'>" \
"      <driver name='tap' type='aio' />" \
"      <source file='%s'/>" \
"      <target dev='%s' bus='xen'/>" \
"    </disk>"

#define VIRT_DOMAIN_CMD_IP "ip=%s:1.2.3.4:%s:255.255.255.0::eth0:off"

#define VIRT_DOMAIN_XML \
"<domain type='xen'>" \
"  <name>%s_%s</name>" \
"  <os>" \
"   <type>linux</type>" \
"   <kernel>/usr/lib/xen/boot/linux-2.6.20-xen-r6</kernel>" \
"   <cmdline> root=/dev/xvda ro %s</cmdline>" \
"  </os>" \
"  <memory>%d</memory>" \
"  <vcpu>%d</vcpu>" \
"  <on_poweroff>destroy</on_poweroff>" \
"  <on_reboot>restart</on_reboot>" \
"  <on_crash>destroy</on_crash>" \
"  <devices>" \
"   %s" \
"  </devices>" \
"</domain>"

#define VIRT_STORAGE_XML \
"<volume type='%s'>" \
"  <name>%s_%s</name>" \
"  <allocation>%lu</allocation>" \
"  <capacity unit='%s'>%lu</capacity>" \
"  <target>" \
"    <path>%s_%s</path>" \
"    <permissions>" \
"      <owner>0744</owner>" \
"      <group>0744</group>" \
"      <mode>0744</mode>" \
"      <label>%s_%s</label>" \
"    </permissions>" \
"  </target>" \
"</volume>"


#include "handler_virt.h"
#include "handler_clusterstats.h"
#include "handler_virt_tender.h"
#include <cherokee/cherokee.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <float.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>



/* Plug-in initialization
 *
 * In this function you can use any of these:
 * http_delete | http_get | http_post | http_put
 *
 * For a full list: cherokee_http_method_t
 *
 * It is what your handler to be implements.
 *
 */
PLUGIN_INFO_HANDLER_EASIEST_INIT (virt, http_get | http_post);


/* Methods implementation
 */
static ret_t 
props_free (cherokee_handler_virt_props_t *props)
{
    cherokee_buffer_mrproper(&props->xsl);
    cherokee_buffer_mrproper(&props->virt);

	return cherokee_handler_clusterstats_props_free (PROP_CLUSTERSTATS(props));
}


ret_t 
cherokee_handler_virt_configure (cherokee_config_node_t *conf, cherokee_server_t *srv, cherokee_module_props_t **_props)
{
	cherokee_list_t *i;
	cherokee_handler_virt_props_t *props;

    /* Instance a new property object
     */

	if (*_props == NULL) {
		CHEROKEE_NEW_STRUCT (n, handler_virt_props);

        cherokee_handler_clusterstats_props_init_base (PROP_CLUSTERSTATS(n), MODULE_PROPS_FREE(props_free));

        /* Look at handler_virt.h
         * This is an virt of configuration.
         */
        n->authenticate = true; /* by default we are secure! */
        n->read_only = true; /* by default we are secure! */
        cherokee_buffer_init (&n->xsl); /* we support a custom header */
        cherokee_buffer_init (&n->virt); /* your first xenserver */

		*_props = MODULE_PROPS(n);
	}

	props = PROP_VIRT(*_props);

	cherokee_config_node_foreach (i, conf) {
		cherokee_config_node_t *subconf = CONFIG_NODE(i);

		if (equal_buf_str (&subconf->key, "authenticate")) {
			props->authenticate = atoi(subconf->val.buf);
		} 
        else if (equal_buf_str (&subconf->key, "read_only")) {
			props->read_only = atoi(subconf->val.buf);
		} 
        else if (equal_buf_str (&subconf->key, "xsl")) {
            cherokee_buffer_add_buffer (&props->xsl, &subconf->val);
        }
        else if (equal_buf_str (&subconf->key, "virt")) {
            cherokee_buffer_add_buffer (&props->virt, &subconf->val);
        }

	}

    /* Init base class
     */

	return cherokee_handler_clusterstats_configure (conf, srv, _props);
}

ret_t 
cherokee_handler_virt_init (cherokee_handler_virt_t *hdl)
{
    cherokee_connection_t *conn = HANDLER_CONN(hdl);

    /* If someone is posting the server should be in
     * read/write mode if this is not the case, just
     * bail out directly */

    if (conn->header.method == http_post && HDL_VIRT_PROPS(hdl)->read_only == TRUE) {
        conn->error_code = http_unauthorized;
        return ret_error;
    }
    
    int isroot = false;
    int len;
    char *this, *next;

    cherokee_buffer_init(&hdl->user);
    cherokee_buffer_init(&hdl->vm);

    hdl->action = nothing;

    cherokee_buffer_add (&conn->pathinfo,
                        conn->request.buf + conn->web_directory.len,
                       conn->request.len - conn->web_directory.len);

    this = conn->pathinfo.buf + 1;

    next = strchr(this, '/'); /* TODO: this code borks! */

    if ((!next && (this && (len = strlen(this)) == 0)) || (next && ((len = next - this) == 0)) )
        hdl->action = showall;
    else {
        cherokee_buffer_add (&hdl->user, this, len);
    }

    if (HDL_VIRT_PROPS(hdl)->authenticate) {
        if (!conn->validator ||
            (conn->validator &&
             (!cherokee_buffer_cmp_buf(&conn->validator->user, &hdl->user) &&
              !(isroot = cherokee_buffer_cmp (&conn->validator->user, "root", 4))))) {
               hdl->action = nothing; /* just in case */
               conn->error_code = http_unauthorized;
               return ret_error;
        }
    } else {
        isroot = true;
    }

    if (hdl->action == showall) {
        if (!isroot) {
            hdl->action = nothing;
            conn->error_code = http_unauthorized;
            return ret_error;
        } else {
            return virt_build_page(hdl);
        }
    }


    if (!next) {
        hdl->action = showuservms;
        return virt_build_page(hdl); 
    } else {
        this = next + 1;
        next = strchr(this, '/');
    
        if ( ( !next && (this && (len = strlen(this)) == 0) ) || (next && ((len = next - this) == 0)) ) {
        //if (!next && (this && (len = strlen(this)) == 0) || (next && ((len = next - this) == 0)) ) {
            hdl->action = showuservms;
            return virt_build_page(hdl);
        }
    }

    cherokee_buffer_add (&hdl->vm, this, len);

    if (!next) {
        hdl->action = domainGetXMLDesc;
        return virt_build_page(hdl);
    } else {
        this = next + 1;
        next = strchr(this, '/');

        if (( !next && (this && (len = strlen(this)) == 0) ) || (next && ((len = next - this) == 0)) ) {
            hdl->action = domainGetXMLDesc;
            return virt_build_page(hdl);
        }
    }

    hdl->action = not_implemented;
    switch (conn->header.method) {
        case http_get:
            if (strncmp(this, "virDomain", 9) == 0) {
                if (strncmp(this+9, "Get", 3) == 0) {
                    if (strcmp(this+12, "ID") == 0) hdl->action = domainGetID;
                    else if (strcmp(this+12, "Name") == 0) hdl->action = domainGetName;
                    else if (strcmp(this+12, "MaxMemory") == 0) hdl->action = domainGetMaxMemory;
                    else if (strcmp(this+12, "MaxVcpus") == 0) hdl->action = domainGetMaxVcpus;
                    else if (strcmp(this+12, "OSType") == 0) hdl->action = domainGetOSType;
                    else if (strcmp(this+12, "UUID") == 0) hdl->action = domainGetUUID;
                    else if (strcmp(this+12, "UUIDString") == 0) hdl->action = domainGetUUIDString;
                    else if (strcmp(this+12, "XMLDesc") == 0) hdl->action = domainGetXMLDesc;
                    else if (strcmp(this+12, "XMLSnapshots") == 0) hdl->action = domainGetXMLSnapshots;
                }
                
                else if (HDL_VIRT_PROPS(hdl)->read_only == FALSE) {
                    if (strcmp(this+9, "Create") == 0) hdl->action = domainCreate;
                    else if (strcmp(this+9, "Destroy") == 0) hdl->action = domainDestroy;
                    else if (strcmp(this+9, "Reboot") == 0) hdl->action = domainReboot;
                    else if (strcmp(this+9, "Shutdown") == 0) hdl->action = domainShutdown;
                
                    else if (strcmp(this+9, "Save") == 0) hdl->action = domainSave;
                    else if (strcmp(this+9, "Restore") == 0) hdl->action = domainRestore;
                    else if (strcmp(this+9, "Migrate") == 0) hdl->action = domainMigrate_args;

                    else if (strcmp(this+9, "AttachDevice") == 0) hdl->action = domainAttachDevice_args;

                    else if (strcmp(this+9, "DefineXML") == 0) hdl->action = domainDefineXML_args;
                    else if (strcmp(this+9, "Undefine") == 0) hdl->action = domainUndefine;
                }
            }
            else if (HDL_VIRT_PROPS(hdl)->read_only == FALSE && strncmp(this, "virStorage", 10) == 0) {
                if (strncmp(this+10, "Vol", 3) == 0) {
                    if (strcmp(this+13, "CreateXML") == 0) hdl->action = storageVolCreateXML_args;
                    else if (strcmp(this+13, "Delete") == 0) hdl->action = storageVolDelete_args;
                    else if (strcmp(this+13, "CloneXML") == 0) hdl->action = storageVolCloneXML_args;
                    else if (strcmp(this+13, "CloneStatus") == 0) hdl->action = storageVolCloneStatus_args;
                    else if (strcmp(this+13, "GetXMLDesc") == 0) hdl->action = storageVolGetXMLDesc_args;
                    else if (strcmp(this+13, "SetPassword") == 0) hdl->action = storageVolSetPassword_args;
                }
            }
            else if (strncmp(this, "virGraph", 8) == 0) {
                if (strcmp(this+8, "Load") == 0) hdl->action = graphLoad_args;
                if (strcmp(this+8, "Interface") == 0) hdl->action = graphInterface_args;
            }
            break;
            
        case http_post: {
            off_t postl;
            cherokee_post_get_len (&conn->post, &postl);

            if (postl <= 0 || postl >= (INT_MAX-1)) {
                TRACE("virt", "post without post");
                conn->error_code = http_bad_request;
                return ret_error;
            }

            if (strncmp(this, "virDomain", 9) == 0) {
                if (strcmp(this+9, "AttachDevice") == 0) hdl->action = domainAttachDevice;
                else if (strcmp(this+9, "DetachDevice") == 0) hdl->action = domainDetachDevice;
                else if (strcmp(this+9, "DefineXML") == 0) hdl->action = domainDefineXML;
            }
            else if (strncmp(this, "virStorage", 10) == 0) {
                if (strncmp(this+10, "Vol", 3) == 0) {
                    if (strcmp(this+13, "CreateXML") == 0) hdl->action = storageVolCreateXML;
                }
            }
 
            break;
        }
            
        default:
            return ret_error;
    }

    if (hdl->action <= 0) {
        TRACE("virt", "There was no action specified");
        conn->error_code = http_bad_request;
        return ret_error;
    }
   
    return virt_build_page(hdl);
}

ret_t
cherokee_handler_virt_free (cherokee_handler_virt_t *hdl)
{
    cherokee_buffer_mrproper (&hdl->buffer);
    cherokee_buffer_mrproper (&hdl->user);
    cherokee_buffer_mrproper (&hdl->vm);

    return ret_ok;
}

ret_t
cherokee_handler_virt_step (cherokee_handler_virt_t *hdl, cherokee_buffer_t *buffer)
{
        cuint_t tosend;

        if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof;

        tosend = (hdl->buffer.len > 1024 ? 1024 : hdl->buffer.len);

        cherokee_buffer_add (buffer, hdl->buffer.buf, tosend);
        cherokee_buffer_move_to_begin (&hdl->buffer, tosend);

        if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof_have_data;

        return ret_ok;
}

ret_t
cherokee_handler_virt_add_headers (cherokee_handler_virt_t *hdl, cherokee_buffer_t *buffer)
{
    cherokee_buffer_add_va (buffer, "Content-Length: %d"CRLF, hdl->buffer.len);

    if (hdl->action > xml)
        cherokee_buffer_add_str (buffer, "Content-Type: application/xml"CRLF);
    else if (hdl->action > graph) {
        cherokee_buffer_add_str (buffer, "Content-Type: image/png"CRLF);
    } else
        cherokee_buffer_add_str (buffer, "Content-Type: text/plain"CRLF);

    return ret_ok;
}

/*
static ret_t
while_func_entries (cherokee_buffer_t *key, void *value, void *param) {
    virConnectPtr conn = NULL;
    cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
    cherokee_buffer_t *buf = (cherokee_buffer_t *)param;

    cherokee_buffer_add_va (&uri, "xen://%s/", value);

    if (HDL_VIRT_PROPS(hdl)->read_only == FALSE && !(conn = virConnectOpen (uri.buf))) {
        if (!(conn = virConnectOpen (uri.buf))) {
            return ret_error;
        }
    }

    if (!conn && !(conn = virConnectOpenReadOnly (uri.buf))) {
        return ret_error;
    } else {
        virDomainPtr dom;
        if (!(dom = virDomainLookupByName(conn, (const char *) key->buf))) {
            return ret_error;
        } else {
            char *xml = virDomainGetXMLDesc(dom, 0);
            cherokee_buffer_add(buf, xml, strlen(xml));
        }
        virConnectClose(conn);
    }

    cherokee_buffer_mrproper(&uri);

    return ret_ok;
}*/

static ret_t load_xml(cherokee_handler_virt_t *hdl, cherokee_buffer_t *xmlDesc) {
    ret_t ret = ret_error;
    FILE *fd;
    cherokee_buffer_t path = CHEROKEE_BUF_INIT;
    cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/index.xml", hdl->user.buf, hdl->vm.buf);

    if ((fd = fopen(path.buf, "r")) != NULL) {
        while (!feof(fd)) {
            char buf[1024];
            size_t amount = fread(buf, sizeof(char), 1024, fd);

            cherokee_buffer_add (xmlDesc, buf, amount);
        }
        fclose(fd);
        if (xmlDesc->len > 0)
            ret = ret_ok;
    }

    cherokee_buffer_mrproper(&path);
    return ret;
}

static ret_t save_xml(cherokee_handler_virt_t *hdl, virDomainPtr result, cherokee_buffer_t *buf) {
    ret_t ret = ret_error;
    FILE *fd;
    cherokee_buffer_t path = CHEROKEE_BUF_INIT;
    cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s", hdl->user.buf);

    mkdir(path.buf, 0755);
    cherokee_buffer_add_va (&path, "/%s", hdl->vm.buf);
    
    mkdir(path.buf, 0755);
    cherokee_buffer_add_str (&path, "/index.xml");

    if ((fd = fopen(path.buf, "w")) == NULL) {
        perror(__func__);
    } else {
        char *output = virDomainGetXMLDesc(result, VIR_DOMAIN_XML_INACTIVE);
        if (output) {
            fwrite(output, strlen(output), sizeof(char), fd);
            if (buf)
                cherokee_buffer_add(buf, output, strlen(output));
            free(output);
        }
        fclose(fd);
        ret = ret_ok;
    }
    cherokee_buffer_mrproper(&path);
    return ret;
}


static ret_t
virt_virt_function(cherokee_handler_virt_t *hdl, virDomainPtr dom, virConnectPtr virConn) {
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    cherokee_buffer_t *buf = &HDL_VIRT(hdl)->buffer;

    switch (hdl->action) {
        /* Returns the status of a clone copy */
        case storageVolCloneStatus_args: {
            ret_t ret;
            void *name;
            struct stat statbuf;
            cherokee_buffer_t queue = CHEROKEE_BUF_INIT;
            
            ret = cherokee_avl_get_ptr (conn->arguments, "name", &name);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCloneXML_args; name argument not specified");
                conn->error_code = http_bad_request;
                return ret_error;
            }
            
            cherokee_buffer_add_va (&queue, "/mnt/images/queue/%s_%s.queued", hdl->user.buf, name);

            if (stat(queue.buf, &statbuf) != 0) {
                conn->error_code = http_not_found;
                ret = ret_error;
            } else {
                char hardlink[1024];
                int namelen;
                if ((namelen = readlink(queue.buf, hardlink, 1023)) == -1) {
                    conn->error_code = http_internal_error;
                    ret = ret_error;
                } else {
                    hardlink[namelen] = '\0';
                    if (stat(hardlink, &statbuf) != 0) {
                        conn->error_code = http_internal_error;
                        ret = ret_error;
                    } else {
                        struct stat statbuf2;
                        cherokee_buffer_t busy = CHEROKEE_BUF_INIT;
                        cherokee_buffer_add_va (&busy, "/mnt/images/queue/%s_%s.busy", hdl->user.buf, name);
                        if (stat(busy.buf, &statbuf2) != 0) {
                            conn->error_code = http_internal_error;
                            ret = ret_error;
                        } else {
                            cherokee_buffer_add_va (buf, "%f", (float)((float)statbuf2.st_size / (float)statbuf.st_size));
                            ret = ret_ok;
                        }
                        cherokee_buffer_mrproper(&busy);
                    }
                }
            }

            cherokee_buffer_mrproper(&queue);
            return ret;
            break;
        }
 
        /* Save the memory of a domain to a file and suspend the domain */
        case domainSave: {
            ret_t ret = ret_ok;
            int result;
            cherokee_buffer_t path = CHEROKEE_BUF_INIT;
            cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/memory", hdl->user.buf, hdl->vm.buf);
            if ((result = virDomainSave(dom, path.buf)) != 0) {
                TRACE("virt", "Saving of %s/%s failed", hdl->user.buf, hdl->vm.buf);
                conn->error_code = http_internal_error;
                ret = ret_error;
            }
            cherokee_buffer_mrproper(&path);

            cherokee_buffer_add_long10(buf, result);
            return ret;
            break;
        }

        /* Restore the memory of a domain from a file and resume the domain */
        case domainRestore: {
            ret_t ret = ret_ok;
            struct stat statbuf;
            cherokee_buffer_t path = CHEROKEE_BUF_INIT;
            cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/memory", hdl->user.buf, hdl->vm.buf);

            if (stat(path.buf, &statbuf) == 0) {
                char *destination = create_tender(HDL_CLUSTERSTATS_PROPS(hdl)->client,
                                                  HDL_CLUSTERSTATS_PROPS(hdl)->threaded_poll,
                                                  virDomainGetName(dom), virDomainGetMaxMemory(dom));

                if (destination == NULL) {
                    TRACE("virt", "Nobody wants %s, poor vm!\n", destination);
                    cherokee_buffer_add_long10(buf, -1);
                } else {
                    virConnectPtr virConnNew;
                    cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
 
                    if (virDomainUndefine(dom) != 0) {
                        TRACE("virt", "Can't undefine %s, some idiot run it already?\n", destination);
                        cherokee_buffer_add_long10(buf, -1);
                    } else {
                        cherokee_buffer_add_va (&uri, "xen://%s/", destination);

                        TRACE("virt", "We will transfer to: %s\n", uri.buf);

                        virConnNew = virConnectOpen (uri.buf);
                                
                        if (virConnNew == NULL) {
                            TRACE("virt", "Can't connect to %s\n", uri.buf);
                            cherokee_buffer_add_long10(buf, -1);
                        } else {
                            cherokee_buffer_add_long10(buf, virDomainRestore(virConnNew, path.buf));
                            virConnectClose(virConn);
                            virConn = virConnNew;
                            /* TODO: Here I actually want virConnNew to be virConn */
                        }
                    }
                    cherokee_buffer_mrproper(&uri);
                    free(destination);
                }
            } else {
                TRACE("virt", "Memory file for %s/%s does not exist", hdl->user.buf, hdl->vm.buf);
                conn->error_code = http_not_found;
                ret = ret_error;
            }

            cherokee_buffer_mrproper(&path);

            return ret;
            break;
        }

        case domainMigrate_args: {
            void *destination;
            ret_t ret;

            ret = cherokee_avl_get_ptr (conn->arguments, "destination", &destination);

            if (ret != ret_ok) {
                destination = create_tender(HDL_CLUSTERSTATS_PROPS(hdl)->client,
                                            HDL_CLUSTERSTATS_PROPS(hdl)->threaded_poll,
                                            virDomainGetName(dom), virDomainGetMaxMemory(dom));
            }

            if (destination == NULL) {
                TRACE("virt", "Nobody wants %s, poor vm!\n", destination);
                cherokee_buffer_add_long10(buf, -1);
            } else {
                virConnectPtr virConnNew;
                cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
                cherokee_buffer_add_va (&uri, "xen://%s/", destination);
                TRACE("virt", "We will transfer to: %s\n", uri.buf);
                virConnNew = virConnectOpen (uri.buf);
                if (virConnNew == NULL) {
                    TRACE("virt", "Can't connect to %s\n", uri.buf);
                    cherokee_buffer_add_long10(buf, -1);
                } else {
                    /* TODO: voodoo oplossen, pointers virConnNew/domNew gaat niet werken */
                    virDomainPtr domNew = virDomainMigrate(dom, virConnNew, VIR_MIGRATE_LIVE, NULL, NULL, 0);

                    if (domNew) {
//                      virDomainFree(dom);
                        dom = domNew;

                        virConnectClose(virConn);
                        virConn = virConnNew;

                        cherokee_buffer_add_long10(buf, 0);
                    } else {
                        cherokee_buffer_add_long10(buf, -1);
                    }                
                }
                cherokee_buffer_mrproper(&uri);
                free(destination);
            }

            return ret_ok;
            break;
        }

        case domainUndefine: {
            int result;
            if ((result = virDomainUndefine(dom)) != 0) {
                conn->error_code = http_internal_error; /* TODO */
            }

            cherokee_buffer_add_long10(buf, result);
            break;
        }

        case domainAttachDevice_args: {
            int result;
            void *temp;
            ret_t ret;

            ret = cherokee_avl_get_ptr (conn->arguments, "type", &temp);

            if (ret == ret_ok) {
                cherokee_buffer_t xml = CHEROKEE_BUF_INIT;

                if (strcmp(temp, "disk") == 0) {
                    void *device;
                    if ((ret = cherokee_avl_get_ptr (conn->arguments, "device", &device)) == ret_ok) {
                        void *file;
                        if ((ret = cherokee_avl_get_ptr (conn->arguments, "file", &file)) == ret_ok) {
                            cherokee_buffer_add_va (&xml, VIRT_DISK_XML, file, device);
                        } else {
                            virStorageVolPtr volume = virt_get_vol_by_args(hdl, virConn, 1);

                            if (volume == NULL) {
                                return ret_error;
                            }
                            
                            file = virStorageVolGetPath(volume);
                            cherokee_buffer_add_va (&xml, VIRT_DISK_XML, file, device);
                            free(file);

                            virStorageVolFree(volume);
                            ret = ret_ok;
                        }
                    }
                }

                else if (strcmp(temp, "interface") == 0) {
                    void *mac, *ip;
                    if ((ret = cherokee_avl_get_ptr (conn->arguments, "mac", &mac)) == ret_ok && (ret = cherokee_avl_get_ptr (conn->arguments, "ip", &ip)) == ret_ok) {
                        cherokee_buffer_add_va (&xml, VIRT_INTERFACE_XML, mac, ip);
                    } else {
                        char *mac = NULL;
                        char *ip  = NULL;
                        cherokee_buffer_t domu = CHEROKEE_BUF_INIT;
                        cherokee_buffer_add_va (&domu, "%s_%s", hdl->user.buf, hdl->vm.buf);
                        if (getNewMac("dv28", domu.buf, &mac, &ip) == 0)
                            cherokee_buffer_add_va (&xml, VIRT_INTERFACE_XML, mac, ip);
                        cherokee_buffer_mrproper(&domu);
                    } 
                }

                if (ret == ret_ok && (result = virDomainAttachDevice(dom, (const char *) xml.buf)) == 0) {
                    ret = ret_ok;
                } else {
                    conn->error_code = http_internal_error;
                    return ret_error;
                }

                cherokee_buffer_add_long10(buf, result);
                cherokee_buffer_mrproper(&xml);
            } else {
                TRACE("virt", "DeviceAttach_args; type was not specified");
                conn->error_code = http_bad_request;
                return ret_error;
            }

            break;
        }

        case domainAttachDevice: {
            off_t postl;
            int result;
            cherokee_buffer_t post = CHEROKEE_BUF_INIT;
            cherokee_post_get_len (&conn->post, &postl);
            cherokee_post_walk_read (&conn->post, &post, (cuint_t) postl);
            if ((result = virDomainAttachDevice(dom, (const char *) post.buf)) != 0)
                conn->error_code = http_internal_error;
            
            cherokee_buffer_mrproper(&post);
            cherokee_buffer_add_long10(buf, result);
            break;
        }


                        case domainGetXMLDesc: {
                            char *xml = virDomainGetXMLDesc(dom, 0);
                            cherokee_buffer_add(buf, xml, strlen(xml));
                            free(xml);
                            break;
                        }

                        case domainGetXMLSnapshots: {
                            /* TODO: maybe in the future one would like to have all snapshots here */
                            struct stat statbuf;
                            cherokee_buffer_t path = CHEROKEE_BUF_INIT;
                        
                            cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/memory", hdl->user.buf, hdl->vm.buf);

                            if (stat(path.buf, &statbuf) != 0) {
                                cherokee_buffer_add_str(buf, "<snapshots />");
                            } else {
                                char *date = ctime(&(statbuf.st_ctime));
                                cherokee_buffer_add_str(buf, "<snapshots>\n");
                                cherokee_buffer_add_va (buf, "  <snapshot at=\"");
                                cherokee_buffer_add (buf, date, strlen(date)-1);
                                cherokee_buffer_add_va (buf, "\" />\n");
                                cherokee_buffer_add_str(buf, "</snapshots>");
                            }

                            cherokee_buffer_mrproper(&path);
                            break;
                        }

                        case domainDetachDevice: {
                            off_t postl;
                            int result;
                            ret_t ret;
                            cherokee_buffer_t post = CHEROKEE_BUF_INIT;
                            cherokee_post_get_len (&conn->post, &postl);
                            cherokee_post_walk_read (&conn->post, &post, (cuint_t) postl);
                            
                            xmlDocPtr doc = xmlParseMemory((const char *) post.buf, post.len);
                            if (doc == NULL) {
                                TRACE("virt", "DeviceAttach; XML document unparceble");
                                conn->error_code = http_bad_request;
                                ret = ret_error;
                            } else 
                                ret = ret_ok;

                            if (ret == ret_ok) {
                                if ((result = virDomainDetachDevice(dom, (const char *) post.buf)) != 0) {
                                    conn->error_code = http_internal_error;
                                    /* TODO: betere afhandeling */
                                }
                                xmlXPathContextPtr context = xmlXPathNewContext(doc);
                                if (context != NULL) {
                                    xmlXPathObjectPtr obj = xmlXPathEval("string(//interface/mac/@address)", context);
                                    xmlXPathFreeContext(context);
                                    if ((obj != NULL) && (obj->type == XPATH_STRING) &&
                                        (obj->stringval != NULL) && (obj->stringval[0] != 0)) {
                                        removeOldMac("dv28", (char *) obj->stringval);
                                    }
                                    xmlXPathFreeObject(obj);
                                }
                            
                                xmlFreeDoc(doc);
                                xmlCleanupParser(); 
                            }
                            
                            cherokee_buffer_mrproper(&post);
                            cherokee_buffer_add_long10(buf, result);
                            break;
                        }

                        case domainGetID: {
                            cherokee_buffer_add_ulong10(buf, virDomainGetID(dom));
                            break;
                        }
                        case domainGetMaxMemory: {
                            cherokee_buffer_add_ulong10(buf, virDomainGetMaxMemory  (dom));
                            break;
                        }
                        case domainGetMaxVcpus: {
                            cherokee_buffer_add_long10(buf, virDomainGetMaxVcpus  (dom));
                            break;
                        }
                        case domainGetName: {
                            const char *name = virDomainGetName (dom);
                            cherokee_buffer_add(buf, name, strlen(name));
                            break;
                        }
                        case domainGetUUID: {
                            unsigned char uuid[VIR_UUID_BUFLEN];
                            if (virDomainGetUUID(dom, uuid) == 0) {
                                cherokee_buffer_add_str(buf, uuid);
                            } else {
                                conn->error_code = http_internal_error;
                                return ret_error;
                            }
                            break;
                        }
                        case domainGetUUIDString: {
                            unsigned char uuid[VIR_UUID_STRING_BUFLEN];
                            if (virDomainGetUUIDString(dom, uuid) == 0) {
                                cherokee_buffer_add_str(buf, uuid);
                            } else {
                                conn->error_code = http_internal_error;
                                return ret_error;
                            }
                            break;
                        }

                        case domainCreate: {
                            char *destination;
                            cherokee_buffer_t xmlDesc = CHEROKEE_BUF_INIT;
                            save_xml(hdl, dom, &xmlDesc);
                            destination = create_tender(HDL_CLUSTERSTATS_PROPS(hdl)->client,
                                                        HDL_CLUSTERSTATS_PROPS(hdl)->threaded_poll,
                                                        virDomainGetName(dom), virDomainGetMaxMemory(dom));

                            if (destination == NULL) {
                                TRACE("virt", "Nobody wants %s, poor vm!\n", destination);
                                cherokee_buffer_add_long10(buf, -1);
                            } else {
                                virConnectPtr virConnNew;
                                cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
 
                                if (virDomainUndefine(dom) != 0) {
                                    TRACE("virt", "Can't undefine %s, some idiot run it already?\n", destination);
                                }
                                       
                                cherokee_buffer_add_va (&uri, "xen://%s/", destination);
                                virConnNew = virConnectOpen (uri.buf);
                                
                                if (virConnNew == NULL) {
                                    TRACE("virt", "Can't connect to %s\n", uri.buf);
                                    cherokee_buffer_add_long10(buf, -1);
                                } else {
                                    cherokee_buffer_add_long10(buf,
                                                               (virDomainCreateLinux(virConnNew, xmlDesc.buf, 0) != NULL ?
                                                                0 : -1 ));
                                    virConnectClose(virConn);

                                    virConn = virConnNew;
                                    /* TODO: Here I actually want virConnNew to be virConn */
                                }
                                cherokee_buffer_mrproper(&uri);
                                free(destination);
                            }
                            
                            cherokee_buffer_mrproper(&xmlDesc);
                            break;
                        }

                        case domainDestroy: {
                            cherokee_buffer_add_long10(buf, virDomainDestroy  (dom));
                            break;
                        }

                        case domainReboot: {
                            cherokee_buffer_add_long10(buf, virDomainReboot  (dom, 0));
                            break;
                        }

                        case domainShutdown: {
                            cherokee_buffer_add_long10(buf, virDomainShutdown  (dom));
                            break;
                        }


                        case domainGetOSType: {
                            char *ostype = virDomainGetOSType(dom);
                            cherokee_buffer_add(buf, ostype, strlen(ostype));
                            free(ostype);
                            break;
                        }


    }


    if (hdl->action == domainUndefine) {
        /* Remove VM data from filesystem */
        cherokee_buffer_t path = CHEROKEE_BUF_INIT;
        cherokee_buffer_t path2 = CHEROKEE_BUF_INIT;
        cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/index.xml", hdl->user.buf, hdl->vm.buf);
        cherokee_buffer_add_buffer (&path2, &path);
        cherokee_buffer_add_str (&path2, ".deleted");
//      unlink(path.buf); /* TODO: instead of delet replace */
        rename(path.buf, path2.buf);
        cherokee_buffer_mrproper(&path);
        cherokee_buffer_mrproper(&path2);
    } else if (hdl->action != domainCreate) {
        save_xml(hdl, dom, NULL);
    }

    return ret_ok;
}

static virStoragePoolPtr
virt_get_pool_by_args(cherokee_handler_virt_t *hdl, virConnectPtr virConn) {
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    virStoragePoolPtr pool;
    void *temp;
    ret_t ret;

    ret = cherokee_avl_get_ptr (conn->arguments, "pool", &temp);
    if (unlikely(ret < ret_ok)) {
        TRACE("virt", "virStoragePoolPtr; Pool argument not specified");
        conn->error_code = http_bad_request;
        return NULL;
    }
    
    pool = virStoragePoolLookupByName(virConn, temp);

    return pool;
}

static virStorageVolPtr
virt_get_vol_by_args(cherokee_handler_virt_t *hdl, virConnectPtr virConn, unsigned short int prefix) {
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    virStoragePoolPtr pool;
    virStorageVolPtr volume;
    void *temp;
    ret_t ret;

    pool = virt_get_pool_by_args(hdl, virConn);

    if (pool == NULL) {
        conn->error_code = http_not_found;
        return NULL;
    }
    
    virStoragePoolRefresh(pool, 0); /* TODO: might be better to do it outside */

    ret = cherokee_avl_get_ptr (conn->arguments, "volume", &temp);
    if (unlikely(ret < ret_ok)) {
        TRACE("virt", "virStorageVolPtr; Volume argument not specified");
        conn->error_code = http_bad_request;
        return NULL;
    }

    if (prefix == 1) {
        cherokee_buffer_t fullvol = CHEROKEE_BUF_INIT;
        cherokee_buffer_add_va (&fullvol, "%s_%s", hdl->user.buf, temp);
        volume = virStorageVolLookupByName(pool, fullvol.buf);
        cherokee_buffer_mrproper(&fullvol);
    } else {
        volume = virStorageVolLookupByName(pool, temp);
    }

    if (volume == NULL)
        conn->error_code = http_not_found;
    
    virStoragePoolFree(pool);

    return volume;
}

/* This function is the home for all functions that need a working
 * pool/volume combination */
static ret_t
virt_pool_vol(cherokee_handler_virt_t *hdl, virConnectPtr virConn) {
    cherokee_buffer_t *buf = &HDL_VIRT(hdl)->buffer;
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    virStorageVolPtr volume;
    ret_t ret = ret_ok;

    /* We only allow clone to run 'unsafe', others get prefixed */
    volume = virt_get_vol_by_args(hdl, virConn, (hdl->action != storageVolCloneXML_args));
    
    /* If the volume doesn't exist, no point to continue */
    if (volume == NULL) {
        conn->error_code = http_not_found;
        return ret_error;
    }

    switch (hdl->action) {
        /* Sets the password of a specific volume, requires the password= option */
        case storageVolSetPassword_args: {
            void *temp;
            ret = cherokee_avl_get_ptr (conn->arguments, "password", &temp);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolSetPassword_args; password argument not specified");
                conn->error_code = http_bad_request;
                ret = ret_error;
                goto virt_pool_vol_cleanup;
            } else {
                cherokee_buffer_t cmd_passwd = CHEROKEE_BUF_INIT;
                cherokee_buffer_add_va (&cmd_passwd, "/usr/sbin/passwdchanger.sh %s %s\n", virStorageVolGetKey(volume), temp);
                cherokee_buffer_add_long10(buf, system(cmd_passwd.buf));
                cherokee_buffer_mrproper(&cmd_passwd);
            }
            break;
        }
       
        /* Removes a volume */
        case storageVolDelete_args: {
            cherokee_buffer_add_long10(buf, virStorageVolDelete(volume, 0));
            break;
        }

        /* Gives a description of a storage volume in XML */
        case storageVolGetXMLDesc_args: {
            char *xml = virStorageVolGetXMLDesc(volume, 0);
            cherokee_buffer_add(buf, xml, strlen(xml));
            free(xml);
            break;
        }

        /* Clones a volume, insecure method! requires a new name= */
        case storageVolCloneXML_args: {
            void *name;
            cherokee_buffer_t busy = CHEROKEE_BUF_INIT;
            
            ret = cherokee_avl_get_ptr (conn->arguments, "name", &name);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCloneXML_args; name argument not specified");
                conn->error_code = http_bad_request;
                ret = ret_error;
                goto virt_pool_vol_cleanup;
            }
            
            cherokee_buffer_add_va (&busy, "/mnt/images/queue/%s_%s.queued", hdl->user.buf, name);

            if (unlikely(symlink(virStorageVolGetKey(volume), busy.buf) == 1)) {
                conn->error_code = http_internal_error;
                ret = ret_error;
            } else {
                cherokee_buffer_add_str(buf, "QUEUED");
            }

            cherokee_buffer_mrproper(&busy);
           
            break;
        }

   }

virt_pool_vol_cleanup:
     /* And in the end we need to free the volume we have used */
    virStorageVolFree(volume);
   
    return ret;
}

static ret_t
virt_virt_new(cherokee_handler_virt_t *hdl, virConnectPtr virConn) {
    cherokee_buffer_t *buf = &HDL_VIRT(hdl)->buffer;
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    cherokee_buffer_t xml = CHEROKEE_BUF_INIT;
    virStoragePoolPtr pool = NULL;
    ret_t ret = ret_ok;
   
    switch (hdl->action) {

        case storageVolCreateXML_args: {
            void *temp, *type, *name, *unit;
            unsigned long int capacity, allocation;

            ret = cherokee_avl_get_ptr (conn->arguments, "pool", &temp);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; pool argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }

            TRACE("args", "%s", temp);

            pool = virStoragePoolLookupByName(virConn, temp);

            if (pool == NULL) {
                conn->error_code = http_not_found;
                goto virt_virt_new_cleanup;
            }
            
            ret = cherokee_avl_get_ptr (conn->arguments, "name", &name);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; name argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }
 
            ret = cherokee_avl_get_ptr (conn->arguments, "type", &type);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; type argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }

            ret = cherokee_avl_get_ptr (conn->arguments, "unit", &unit);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; unit argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }
            
            ret = cherokee_avl_get_ptr (conn->arguments, "allocation", &temp);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; allocation argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }
           
            allocation = strtoul(temp, NULL, 10);
            if (errno == ERANGE) {
                TRACE("virt", "storageVolCreateXML_args; allocation is not a number");
                conn->error_code = http_bad_request;
                ret = ret_error;
                goto virt_virt_new_cleanup;
            }

            ret = cherokee_avl_get_ptr (conn->arguments, "capacity", &temp);
            if (unlikely(ret < ret_ok)) {
                TRACE("virt", "storageVolCreateXML_args; capacity argument not specified");
                conn->error_code = http_bad_request;
                goto virt_virt_new_cleanup;
            }
            
            capacity = strtoul(temp, NULL, 10);
            if (errno == ERANGE || capacity == 0) {
                TRACE("virt", "storageVolCreateXML_args; capacity is not a number");
                conn->error_code = http_bad_request;
                ret = ret_error;
                goto virt_virt_new_cleanup;
            }

            cherokee_buffer_add_va (&xml, VIRT_STORAGE_XML, type, hdl->user.buf, name, allocation, unit, capacity, hdl->user.buf, name, hdl->user.buf, name);
            break;
        }

        case domainDefineXML_args: {
            ret_t ret;
            unsigned int i;
            unsigned long vcpu = 0, interface = 0, memory = 0;

            void *temp = NULL;

            ret = cherokee_avl_get_ptr (conn->arguments, "vcpu", &temp);
            if (ret == ret_ok)
                vcpu = strtoul(temp, (char **) NULL, 10);

            if (ret != ret_ok || errno == ERANGE || vcpu == 0) {
                conn->error_code = http_internal_error;
                goto virt_virt_new_cleanup;
            }


            ret = cherokee_avl_get_ptr (conn->arguments, "memory", &temp);
            if (ret == ret_ok)
                memory = strtoul(temp, (char **) NULL, 10);

            if (ret != ret_ok || errno == ERANGE || memory == 0) {
                conn->error_code = http_internal_error;
                goto virt_virt_new_cleanup;
            }


            ret = cherokee_avl_get_ptr (conn->arguments, "interface", &temp);
            if (ret == ret_ok)
                interface = strtoul(temp, (char **) NULL, 10);
            
            if (ret != ret_ok || errno == ERANGE) {
                conn->error_code = http_internal_error;
                goto virt_virt_new_cleanup;
            }

            cherokee_buffer_t cmdline_extra = CHEROKEE_BUF_INIT;
            cherokee_buffer_t xml_interfaces = CHEROKEE_BUF_INIT;
            cherokee_buffer_t domu = CHEROKEE_BUF_INIT;
            cherokee_buffer_add_va (&domu, "%s_%s", hdl->user.buf, hdl->vm.buf);

            for (i = 0; i < interface; i++) {
                char *mac = NULL;
                char *ip  = NULL;
                if (getNewMac("dv28", domu.buf, &mac, &ip) == 0) {
                    cherokee_buffer_add_va (&xml_interfaces, VIRT_INTERFACE_XML, mac, ip);
                    if (i == 0) {
                        /* TODO: terrible hack */
                        char gateway[16];
                        char *temp;
                        strcpy(gateway, ip);
                        temp = strchr(gateway, '.');
                        temp = strchr(++temp, '.');
                        temp = strchr(++temp, '.');
                        strcpy(++temp, "254");
                        cherokee_buffer_add_va (&cmdline_extra, VIRT_DOMAIN_CMD_IP, ip, gateway);
                    }
                }
            }

            cherokee_buffer_mrproper(&domu);

            cherokee_buffer_add_va (&xml, VIRT_DOMAIN_XML,
                hdl->user.buf, hdl->vm.buf, cmdline_extra.buf, memory, vcpu, xml_interfaces.buf);

            cherokee_buffer_mrproper(&xml_interfaces);
            cherokee_buffer_mrproper(&cmdline_extra);
            break;
        }

        case storageVolCreateXML:
        case domainDefineXML: {
            off_t postl;
            cherokee_post_get_len (&conn->post, &postl);
            cherokee_post_walk_read (&conn->post, &xml, (cuint_t) postl);
            break;
        }
    }

    switch (hdl->action) {
        case domainDefineXML_args:
        case domainDefineXML: {
            virDomainPtr result = virDomainDefineXML(virConn, (const char *) xml.buf);

            if (result == NULL) {
                /* TODO: vrij maken eventuele uitgegeven macs! */
                conn->error_code = http_internal_error;
                goto virt_virt_new_cleanup;
            }
            
            save_xml(hdl, result, buf);

            virDomainFree(result);

            break;
        }

        case storageVolCreateXML_args:
        case storageVolCreateXML: {
            virStorageVolPtr vol = virStorageVolCreateXML(pool, xml.buf, 0);

            if (vol == NULL) {
                cherokee_buffer_add_long10(buf, -1);
                goto virt_virt_new_cleanup;
            }
        
            virStorageVolFree(vol);

            cherokee_buffer_add_long10(buf, 0);
            break;
        }
    }

virt_virt_new_cleanup:
    cherokee_buffer_mrproper(&xml);

    if (pool)
        virStoragePoolFree(pool);

    return ret;
}


static ret_t
virt_virt_do(cherokee_handler_virt_t *hdl, cherokee_buffer_t *domu, cherokee_buffer_t *uri)
{
    cherokee_connection_t *conn = HANDLER_CONN(hdl);

    ret_t ret = ret_error;
    virConnectPtr virConn = NULL;


    if (HDL_VIRT_PROPS(hdl)->read_only == FALSE)
        virConn = virConnectOpen (uri->buf);

    if (!virConn && !(virConn = virConnectOpenReadOnly (uri->buf))) {
        conn->error_code = http_service_unavailable;
        return ret_error;
    }

    switch (hdl->action) {
        case storageVolDelete_args:
        case storageVolSetPassword_args:
        case storageVolGetXMLDesc_args:
        case storageVolCloneXML_args:
            ret = virt_pool_vol(hdl, virConn);
            break;

        case storageVolCreateXML_args:
        case storageVolCreateXML:
        case domainDefineXML_args:
        case domainDefineXML:
            ret = virt_virt_new(hdl, virConn);
            break;

        default: {
            virDomainPtr dom;
            if ((dom = virDomainLookupByName(virConn, domu->buf)) == NULL) {
                conn->error_code = http_not_found;
            } else {
                ret = virt_virt_function(hdl, dom, virConn);
                virDomainFree(dom);
            }
        }
    }
    
    virConnectClose(virConn);
    return ret;
}

static ret_t
virt_while (cherokee_buffer_t *key, void *value, void *param) {
    cherokee_handler_virt_t * hdl = param;
//    cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
//    cherokee_buffer_add_va (&uri, "xen://%s/", ((cherokee_buffer_t *) value)->buf);
//    virt_virt_do((cherokee_handler_virt_t *) param, key, &uri);
//    cherokee_buffer_mrproper(&uri);
    
    cherokee_buffer_add_va (&hdl->buffer, "<domain><name>%s</name></domain>", key->buf);

    return ret_ok;
}

static ret_t
virt_while_user (cherokee_buffer_t *key, void *value, void *param) {
    cherokee_handler_virt_t *hdl = param;
    if (key->len > hdl->user.len && key->buf[hdl->user.len] == '_' && strncmp(key->buf, hdl->user.buf, hdl->user.len) == 0)
        return virt_while (key, value, param);

    return ret_ok;
}

static ret_t 
virt_build_page (cherokee_handler_virt_t *hdl)
{
    ret_t              ret;
    cherokee_connection_t *conn = HANDLER_CONN(hdl);
    cherokee_buffer_t uri = CHEROKEE_BUF_INIT;
    cherokee_buffer_t domu = CHEROKEE_BUF_INIT;
    
    /* We use the webserver methods to parse the querystring */
    /* Maybe do something smart with ENUM, begin_args, end_args... */
    if ((hdl->action == domainDefineXML_args) ||
        (hdl->action == domainAttachDevice_args) ||
        (hdl->action == domainMigrate_args) ||
        (hdl->action == storageVolGetXMLDesc_args) ||
        (hdl->action == storageVolDelete_args) ||
        (hdl->action == storageVolSetPassword_args) ||
        (hdl->action == storageVolCloneXML_args) ||
        (hdl->action == storageVolCloneStatus_args) ||
        (hdl->action == storageVolCreateXML_args) ||
        (hdl->action == graphLoad_args) ||
        (hdl->action == graphInterface_args)) {
        ret = cherokee_connection_parse_args (conn);
        if (unlikely(ret < ret_ok)) {
            conn->error_code = http_internal_error;
            return ret_error;
        }
    }

    switch (hdl->action) {
        case graphInterface_args: {
            struct stat statbuf;
            cherokee_buffer_t path = CHEROKEE_BUF_INIT;
            void *interface;
            if ((ret = cherokee_avl_get_ptr (conn->arguments, "interface", &interface)) != ret_ok)
                interface = "eth0";

            cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/interface_%s.rrd", hdl->user.buf, hdl->vm.buf, interface);
            if (stat(path.buf, &statbuf) != 0) {
                conn->error_code = http_not_found;
                ret = ret_error;
            } else {
                 void *width, *height, *start, *end;
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "width", &width)) != ret_ok)
                    width = "600";
                
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "height", &height)) != ret_ok)
                    height = "200";

                if ((ret = cherokee_avl_get_ptr (conn->arguments, "start", &start)) != ret_ok)
                    start = "now-1h";
                
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "end", &end)) != ret_ok)
                    end = "now";

                cherokee_buffer_t def1 = CHEROKEE_BUF_INIT;
                cherokee_buffer_t def2 = CHEROKEE_BUF_INIT;
                cherokee_buffer_add_va (&def1, "DEF:rxbytes=%s:rxbytes:AVERAGE:step=30", path.buf);
                cherokee_buffer_add_va (&def2, "DEF:txbytes=%s:txbytes:AVERAGE:step=30", path.buf);
                char **calcpr  = NULL;
                int xsize, ysize;
                double ymin, ymax;
                /* TODO: betere random hier */
                char filenametemp[L_tmpnam+1];
                char *filename = tmpnam(filenametemp);

//                char *filename = mktemp(strdup("handler_virt_XXXXXX"));
                char *r_graph[] = { "rrdgraph", filename,
                                    "-a", "PNG",
                                    "-w", width,
                                    "-h", height,
                                    "--start", start,
                                    "--end", end,
                                    "--title", interface,
                                    "--lower-limit", "0", "--alt-autoscale-max", "--vertical-label", "bits per second",
                                    def1.buf, 
                                    def2.buf,
                                    "CDEF:txbits=txbytes,8,*", 
                                    "CDEF:rxbits=rxbytes,8,*",
                                    "AREA:rxbits#00EE00:rxbits",
                                    "LINE:txbits#0000EE:txbits" };

                rrd_graph(25, r_graph, &calcpr, &xsize, &ysize, NULL, &ymin, &ymax);
                if ((ret = cherokee_buffer_read_file(&hdl->buffer, filename)) == ret_error) {
                    hdl->action = http_internal_error;
                }

                unlink(filename);

                cherokee_buffer_mrproper(&def1);
                cherokee_buffer_mrproper(&def2);
            }
            cherokee_buffer_mrproper(&path);
            goto virt_build_page_cleanup;
 
               

        };
        case graphLoad_args: {
            struct stat statbuf;
            cherokee_buffer_t path = CHEROKEE_BUF_INIT;
            cherokee_buffer_add_va (&path, "/mnt/netapp/users/%s/%s/cpuTime.rrd", hdl->user.buf, hdl->vm.buf);
            if (stat(path.buf, &statbuf) != 0) {
                conn->error_code = http_not_found;
                ret = ret_error;
            } else {
                void *width, *height, *start, *end;
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "width", &width)) != ret_ok)
                    width = "600";
                
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "height", &height)) != ret_ok)
                    height = "200";

                if ((ret = cherokee_avl_get_ptr (conn->arguments, "start", &start)) != ret_ok)
                    start = "now-1h";
                
                if ((ret = cherokee_avl_get_ptr (conn->arguments, "end", &end)) != ret_ok)
                    end = "now";

                /* TODO: wat error checking? */

                cherokee_buffer_t def = CHEROKEE_BUF_INIT;
                cherokee_buffer_add_va (&def, "DEF:cputime=%s:cpuTime:AVERAGE:step=30", path.buf);
                char **calcpr  = NULL;
                int xsize, ysize;
                double ymin, ymax;
                char *filename = mktemp(strdup("handler_virt_XXXXXX"));
                char *r_graph[] = { "rrdgraph",
                                    filename,
                                    "-a", "PNG",
                                    "-w", width,
                                    "-h", height,
                                    "--start", start,
                                    "--end", end,
                                    "--title", hdl->vm.buf,
                                    "--lower-limit", "0", "--alt-autoscale-max", "--vertical-label", "(used / total) time",
                                    def.buf, 
                                    "CDEF:cpuload=cputime,1000000000,/", 
                                    "LINE:cpuload#EE0000:cpuLoad" };

                rrd_graph(22, r_graph, &calcpr, &xsize, &ysize, NULL, &ymin, &ymax);
                if ((ret = cherokee_buffer_read_file(&hdl->buffer, filename)) == ret_error) {
                    hdl->action = http_internal_error;
                }

                unlink(filename);
                free(filename);

                cherokee_buffer_mrproper(&def);
            }
            cherokee_buffer_mrproper(&path);
            goto virt_build_page_cleanup;
        }
    }
    
    if (hdl->action > xml && HDL_VIRT_PROPS(hdl)->xsl.len > 0)
        cherokee_buffer_add_va (&hdl->buffer, "<?xml version=\"1.0\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"%s\"?>\n", HDL_VIRT_PROPS(hdl)->xsl.buf);


    switch (hdl->action) {
        case showall: {
            size_t len;
            if (cherokee_avl_r_len(&HDL_CLUSTERSTATS_PROPS(hdl)->entries, &len) == ret_ok && len > 0) {
                cherokee_buffer_add_str (&hdl->buffer, "<domains>\n");
                cherokee_avl_r_while (&HDL_CLUSTERSTATS_PROPS(hdl)->entries, (cherokee_avl_while_func_t) cherokee_handler_clusterstats_while_func_entries, (void *) &hdl->buffer, NULL, NULL);
                cherokee_buffer_add_str (&hdl->buffer, "</domains>");
            } else {
                cherokee_buffer_add_str (&hdl->buffer, "<domains/>");
            }
        
            ret = ret_ok;
            break;
        }

        case showuservms: {
            cherokee_buffer_add_str (&hdl->buffer, "<domains>\n");
            get_all_configurations(&HDL_CLUSTERSTATS_PROPS(hdl)->entries, &hdl->user, &hdl->buffer);
            cherokee_buffer_add_str (&hdl->buffer, "</domains>");
            ret = ret_ok;
            break;
        }

        default: {
            AvahiStringList *list = NULL;
            cherokee_buffer_add_va (&domu, "%s_%s", hdl->user.buf, hdl->vm.buf);

            ret = cherokee_avl_r_get(&HDL_CLUSTERSTATS_PROPS(hdl)->entries, &domu, (void **) &list);

            if (ret == ret_not_found) {
                virDomainPtr virDom;
                virConnectPtr virConn;
                if (HDL_VIRT_PROPS(hdl)->virt.len > 0)
                    cherokee_buffer_add_va (&uri, "xen://%s/", HDL_VIRT_PROPS(hdl)->virt); // TODO: change!

                /* If we have the read only parameter, we will set up a connection to the
                 * Hypervisor here. */
                if (HDL_VIRT_PROPS(hdl)->read_only == FALSE)
                    virConn = virConnectOpen (uri.buf);

                /* We should already have a connection (read only) or build a new connection
                 * if this doesn't work, we can safely assume our services is fubar */
                if (!virConn && !(virConn = virConnectOpenReadOnly (uri.buf))) {
                    conn->error_code = http_service_unavailable;
                    return ret_error;
                }

                /* We lookup if there is a domain somewhere with this name */
                if ((virDom = virDomainLookupByName(virConn, domu.buf)) == NULL) {
                    /* If the domain is not found on the network the only possible
                     * command that is possible would be to Define it */
                    if (hdl->action == domainDefineXML_args || hdl->action == domainDefineXML) {
                        ret = ret_ok;
                    } else {
                        /* We should also look on disk for defined stuff */
                        cherokee_buffer_t xmlDesc = CHEROKEE_BUF_INIT;
                        if (load_xml(hdl, &xmlDesc) == ret_ok) {
                            if ((virDom = virDomainDefineXML(virConn, xmlDesc.buf)) != NULL) {
                                /* The domain existed and is loaded! */
                                ret = ret_ok;
                            } else {
                                hdl->action = nothing;
                                conn->error_code = http_internal_error;
                                ret = ret_error;
                            }
                        } else {    
                            /* Otherwise we don't have anything to do and we should
                             * return an error */
                            hdl->action = nothing;
                            conn->error_code = http_not_found;
                            ret = ret_error;
                        }

                        cherokee_buffer_mrproper(&xmlDesc);
                    }
                } else {
                    /* We don't want te recreate things that already found on the network
                     * first undefine them! */
                    if (hdl->action == domainDefineXML_args || hdl->action == domainDefineXML) {
                        TRACE("virt", "domainDefineXML_args/domainDefineXML; domain already exists");
                        hdl->action = nothing;
                        conn->error_code = http_bad_request;
                        ret = ret_error;
                    } else {
                        /* Everything is ok, because nothing is found. */
                        ret = ret_ok;
                    }

                    /* Domain wasn't NULL, so we should free it here */
                    virDomainFree(virDom);
                }
                virConnectClose (virConn);
            } else if (ret == ret_ok) {
                char *hostname;
                avahi_string_list_get_pair(avahi_string_list_find(list, "dom0"), NULL, &hostname, NULL); 
                cherokee_buffer_add_va (&uri, "xen://%s/", hostname);
                printf("%s\n", uri.buf);
                avahi_free(hostname);
            } else {
                hdl->action = http_internal_error;
                hdl->action = nothing;
                ret = ret_error;
            }
        }

    }

    if (ret == ret_ok && hdl->action != showall && hdl->action != showuservms) {
        ret = virt_virt_do(hdl, &domu, &uri);
    }

virt_build_page_cleanup:
    cherokee_buffer_mrproper(&domu);
    cherokee_buffer_mrproper(&uri);

    return ret;
}


ret_t
cherokee_handler_virt_new  (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props)
{
	ret_t ret;
    CHEROKEE_NEW_STRUCT (n, handler_virt);

    /* Init the base class
     */

    cherokee_handler_init_base(HANDLER(n), cnt, HANDLER_PROPS(props), PLUGIN_INFO_HANDLER_PTR(virt));

    MODULE(n)->init         = (handler_func_init_t) cherokee_handler_virt_init;
    MODULE(n)->free         = (module_func_free_t) cherokee_handler_virt_free;
    HANDLER(n)->step        = (handler_func_step_t) cherokee_handler_virt_step;
    HANDLER(n)->add_headers = (handler_func_add_headers_t) cherokee_handler_virt_add_headers;

    HANDLER(n)->support = hsupport_length | hsupport_range;

    ret = cherokee_buffer_init (&n->buffer);
    if (unlikely(ret != ret_ok))
        return ret;

    ret = cherokee_buffer_ensure_size (&n->buffer, 4*1024);
    if (unlikely(ret != ret_ok))
        return ret;

    *hdl = HANDLER(n);

    return ret_ok;
}

