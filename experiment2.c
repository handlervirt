#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    char filemac[13];
    unsigned long int mac;
    int fd = open("/mnt/netapp/maccounter", O_RDWR|O_CREAT|O_TRUNC);
    flock(fd, LOCK_EX);
    FILE * fd2 = fdopen(fd, "r");

    if (fd2 == NULL) printf("ga dood\n");
    if (fread(filemac, sizeof(char), 13, fd2) == 0) {
            printf("read = 0\n");
        mac = 0x0;
    } else {
        mac++;
    }

    mac = strtoul(filemac, NULL, 16);
    printf("cur: %06X\n", mac);
    mac++;
    snprintf(filemac, 13, "%06X", mac);
    printf("next: %s\n", filemac);

    lseek(fd, 0, SEEK_SET);
    write(fd, &filemac, 13);
    close(fd);
    flock(fd, LOCK_UN);
}
