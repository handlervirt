#include <float.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>
#include <avahi-common/thread-watch.h>

typedef struct {
    const char *name;
    char *dom;
    float cost;
} tender_t;

char * create_tender(AvahiClient *client, AvahiThreadedPoll *threaded_poll, const char *name, unsigned long memory);
