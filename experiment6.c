#include <stdio.h>

int main() {
    FILE *fd = fopen("/mnt/netapp/bigtest", "w");
    
    if (fd < 0) {
        perror("fopen");
        return -1;
    }

    fseek(fd, 1024L*1024L*1024L, SEEK_CUR);
    
    fwrite("\0", 1, sizeof(char), fd);

    fclose(fd);
    return 0;
}
