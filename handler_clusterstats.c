/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "handler_clusterstats.h"
#include <cherokee/cherokee.h>

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>

#include <avahi-common/malloc.h>
#include <avahi-common/error.h>

/* Plug-in initialization
 *
 * In this function you can use any of these:
 * http_delete | http_get | http_post | http_put
 *
 * For a full list: cherokee_http_method_t
 *
 * It is what your handler to be implements.
 *
 */
PLUGIN_INFO_HANDLER_EASIEST_INIT (clusterstats, http_get);


/* Methods implementation
 */

ret_t 
cherokee_handler_clusterstats_props_free (cherokee_handler_clusterstats_props_t *props)
{
    if (props->threaded_poll)
       avahi_threaded_poll_stop(props->threaded_poll);

    if (props->sb)
        avahi_service_browser_free(props->sb);

    if (props->client)
       avahi_client_free(props->client);

    if (props->threaded_poll)
        avahi_threaded_poll_free(props->threaded_poll);

    cherokee_buffer_mrproper(&props->service_type);

    cherokee_avl_r_mrproper (&props->entries, (cherokee_func_free_t) cherokee_buffer_mrproper);

	return cherokee_module_props_free_base (MODULE_PROPS(props));
}


ret_t 
cherokee_handler_clusterstats_configure (cherokee_config_node_t *conf, cherokee_server_t *srv, cherokee_module_props_t **_props)
{
	cherokee_list_t                      *i;
	cherokee_handler_clusterstats_props_t *props;

	if (*_props == NULL) {
		CHEROKEE_NEW_STRUCT (n, handler_clusterstats_props);

		cherokee_module_props_init_base (MODULE_PROPS(n), 
						 MODULE_PROPS_FREE(cherokee_handler_clusterstats_props_free));
        
        /* Look at handler_clusterstats.h
         * This is an avahi of configuration.
         */
        cherokee_buffer_init (&n->service_type);

        cherokee_avl_r_init(&n->entries);

        n->client = NULL;
        n->sb = NULL;
        n->threaded_poll = NULL;

		*_props = MODULE_PROPS(n);
	}


	props = PROP_CLUSTERSTATS(*_props);

    cherokee_avl_r_init(&props->entries);

	cherokee_config_node_foreach (i, conf) {
		cherokee_config_node_t *subconf = CONFIG_NODE(i);

        if (equal_buf_str (&subconf->key, "service_type")) {
            cherokee_buffer_add_buffer (&props->service_type, &subconf->val);
		} 
	}
    
    /* If the configuration for avahi was not set, we should not continue */
    if (props->service_type.len == 0) {
        return ret_error;
    }

    if (!(props->threaded_poll = avahi_threaded_poll_new())) {
        return ret_error;
    }

    client_callback(NULL, AVAHI_CLIENT_FAILURE, props);

    /* Finally, start the event loop thread */
    if (avahi_threaded_poll_start(props->threaded_poll) < 0) {
        return ret_error;
    }


	return ret_ok;
}

ret_t
cherokee_handler_clusterstats_new  (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props)
{
	ret_t ret;

	CHEROKEE_NEW_STRUCT (n, handler_clusterstats);
	
	/* Init the base class object
	 */
	cherokee_handler_init_base(HANDLER(n), cnt, HANDLER_PROPS(props), PLUGIN_INFO_HANDLER_PTR(clusterstats));
	   
	MODULE(n)->init         = (handler_func_init_t) cherokee_handler_clusterstats_init;
	MODULE(n)->free         = (module_func_free_t) cherokee_handler_clusterstats_free;
	HANDLER(n)->step        = (handler_func_step_t) cherokee_handler_clusterstats_step;
	HANDLER(n)->add_headers = (handler_func_add_headers_t) cherokee_handler_clusterstats_add_headers;

	HANDLER(n)->support = hsupport_length | hsupport_range;

	/* Init
	 */
	ret = cherokee_buffer_init (&n->buffer);
	if (unlikely(ret != ret_ok)) 
		return ret;

	ret = cherokee_buffer_ensure_size (&n->buffer, 4*1024);
	if (unlikely(ret != ret_ok)) 
		return ret;

	*hdl = HANDLER(n);

	return ret_ok;
}


ret_t 
cherokee_handler_clusterstats_free (cherokee_handler_clusterstats_t *hdl)
{
	cherokee_buffer_mrproper (&hdl->buffer);

	return ret_ok;
}

ret_t
cherokee_handler_clusterstats_while_func_entries (cherokee_buffer_t *key, void *value, void *param) {
    cherokee_buffer_t *buf = (cherokee_buffer_t *)param;
    AvahiStringList *list  = (AvahiStringList *)value;
    char *avahi_key, *avahi_value;
    unsigned int len;
    
    cherokee_buffer_add_str (buf, "  <domain status=\"running\">\n");
    cherokee_buffer_add_va  (buf, "    <name>%s</name>\n", key->buf);

    if ((len = avahi_string_list_length (list)) > 0) {
    unsigned int i;
    cherokee_buffer_add_str (buf, "    <properties>\n");

    for (i = 0; i < len; i++) {
        avahi_string_list_get_pair (list, &avahi_key, &avahi_value, NULL);
        cherokee_buffer_add_va  (buf, "      <property name=\"%s\">%s</property>\n", avahi_key, avahi_value);
        avahi_free(avahi_key);
        avahi_free(avahi_value);
        list = avahi_string_list_get_next(list);
    }

    cherokee_buffer_add_str (buf, "    </properties>\n");
    } else {
    cherokee_buffer_add_str (buf, "    <properties/>\n");
    }

    cherokee_buffer_add_str (buf, "  </domain>\n");

    return ret_ok;
}

static void
clusterstats_build_page (cherokee_handler_clusterstats_t *hdl)
{
    cherokee_buffer_t *buf;
    size_t len;

    /* Init
     */
    buf = &hdl->buffer;

    /* Useful output
     */

    /* First, block the event loop */
//    avahi_threaded_poll_lock(HDL_CLUSTERSTATS_PROPS(hdl)->threaded_poll);

    /* Than, do your stuff */
    if (cherokee_avl_r_len(&HDL_CLUSTERSTATS_PROPS(hdl)->entries, &len) == ret_ok && len > 0) {
        cherokee_buffer_add_str (buf, "<domains>\n");
        cherokee_avl_r_while (&HDL_CLUSTERSTATS_PROPS(hdl)->entries, (cherokee_avl_while_func_t) cherokee_handler_clusterstats_while_func_entries, (void *) buf, NULL, NULL);
        cherokee_buffer_add_str (buf, "</domains>");
    } else {
        cherokee_buffer_add_str (buf, "<domains/>");
    }

    /* Finally, unblock the event loop */
//    avahi_threaded_poll_unlock(HDL_CLUSTERSTATS_PROPS(hdl)->threaded_poll);
}

ret_t 
cherokee_handler_clusterstats_init (cherokee_handler_clusterstats_t *hdl)
{
	cint_t  web_interface = 1;

	/* Build the page
	 */
	if (web_interface) {
		clusterstats_build_page (hdl);
	}

	hdl->action = send_page;
	
	return ret_ok;
}


ret_t 
cherokee_handler_clusterstats_step (cherokee_handler_clusterstats_t *hdl, cherokee_buffer_t *buffer)
{
        cuint_t tosend;

        if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof;

        tosend = (hdl->buffer.len > 1024 ? 1024 : hdl->buffer.len);

        cherokee_buffer_add (buffer, hdl->buffer.buf, tosend);
        cherokee_buffer_move_to_begin (&hdl->buffer, tosend);

        if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof_have_data;

        return ret_ok;
}


ret_t 
cherokee_handler_clusterstats_add_headers (cherokee_handler_clusterstats_t *hdl, cherokee_buffer_t *buffer)
{
    if (hdl->buffer.len == 0) {
        cherokee_connection_t *conn = HANDLER_CONN(hdl);
        conn->error_code = http_not_found;
    }

	cherokee_buffer_add_va (buffer, "Content-Length: %d"CRLF, hdl->buffer.len);

	switch (hdl->action) {
    	case send_page:
    	default:
	    	cherokee_buffer_add_str (buffer, "Content-Type: application/xml"CRLF);
		    break;
	}

	return ret_ok;
}


/* Avahi example stuff */

static void resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    void* userdata) {
    cherokee_handler_clusterstats_props_t *props = userdata;
    assert(r);

    /* Called whenever a service has been resolved successfully or timed out */

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            TRACE("avahi", "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
             avahi_service_resolver_free(r);
            break;

        case AVAHI_RESOLVER_FOUND: {
            char a[AVAHI_ADDRESS_STR_MAX];
            AvahiStringList *val;
            AvahiStringList *nonvol = avahi_string_list_copy(txt);

            avahi_address_snprint(a, sizeof(a), address);

            
            cherokee_buffer_t buf = CHEROKEE_BUF_INIT;
            cherokee_buffer_add(&buf, name, strlen(name));
            
            if (cherokee_avl_r_del(&props->entries, &buf, (void *) &val) == ret_ok) {
                avahi_string_list_free(val);
            }

            nonvol = avahi_string_list_add_pair(nonvol, "dom0", host_name);

            cherokee_avl_r_add(&props->entries, &buf, (void* ) nonvol);
            cherokee_buffer_mrproper(&buf);

            TRACE("avahi", "Service '%s' of type '%s' in domain '%s':\n", name, type, domain);
        }
    }
}

static void browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    void* userdata) {
    cherokee_handler_clusterstats_props_t *props = userdata;
    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */

    switch (event) {
        case AVAHI_BROWSER_FAILURE:            
            TRACE("avahi", "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_service_browser_free(b);
            return;

        case AVAHI_BROWSER_NEW:
            TRACE("avahi", "(Browser) NEW: service '%s' of type '%s' in domain '%s'\n", name, type, domain);

            /* We ignore the returned resolver object. In the callback
               function we free it. If the server is terminated before
               the callback function is called the server will free
               the resolver for us. */

            if (!(avahi_service_resolver_new(avahi_service_browser_get_client(b), interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, 0, resolve_callback, userdata)))
                TRACE("avahi", "Failed to resolve service '%s': %s\n", name, avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            
            break;

        case AVAHI_BROWSER_REMOVE: {
            void *val;
            cherokee_buffer_t buf = CHEROKEE_BUF_INIT;
            cherokee_buffer_add(&buf, name, strlen(name));
            if (cherokee_avl_r_del(&props->entries, &buf, &val) == ret_ok) {
                avahi_string_list_free((AvahiStringList*) val);
            }
            cherokee_buffer_mrproper(&buf);

//            cherokee_avl_r_del_ptr(&PROP_CLUSTERSTATS(userdata)->entries, name, val);

            TRACE("avahi", "(Browser) REMOVE: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
            break;
        }
        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
            TRACE("avahi", "(Browser) %s\n", event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
            break;
    }
}

static void client_callback(AvahiClient *c, AvahiClientState state, void * userdata) {
    cherokee_handler_clusterstats_props_t *props = userdata;

    /* Called whenever the client or server state changes */

    switch (state) {
        case AVAHI_CLIENT_S_RUNNING: {
            if (!(props->sb = avahi_service_browser_new(c, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, props->service_type.buf, NULL, 0, browse_callback, props))) {
                TRACE ("avahi", "Failed to create service browser: %s (%s)\n", avahi_strerror(avahi_client_errno(c)), props->service_type.buf);
            }
            break;
        }

        case AVAHI_CLIENT_FAILURE: {
            int error;
            if (c)
                avahi_client_free(c);

            props->client = avahi_client_new(avahi_threaded_poll_get(props->threaded_poll), AVAHI_CLIENT_NO_FAIL, client_callback, props, &error);
            break;
        }

        default:
            break;
    }
}

ret_t
cherokee_handler_clusterstats_props_init_base (cherokee_handler_clusterstats_props_t *props, module_func_props_free_t free_func)
{
    cherokee_buffer_init (&props->service_type);

    return cherokee_handler_props_init_base (HANDLER_PROPS(props), free_func);
}
