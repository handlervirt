from Form import *
from Table import *
from Module import *
from validations import *

NOTE_AUTHENTICATE   = 'Use authentication when a request is made.'
NOTE_READ_ONLY      = 'Only connect to libvirt using a readonly connection.'
NOTE_SERVICE_TYPE   = 'What service type should we browse on the network for.'
NOTE_XSL            = 'You can make a custom stylesheet for the XML output of libvirt.'

class ModuleVirtBase (Module, FormHelper):
    PROPERTIES = [
        'service_type',
        'authenticate',
        'read_only',
        'xsl'
    ]

    def __init__ (self, cfg, prefix, name, submit_url):
        FormHelper.__init__ (self, name, cfg)
        Module.__init__ (self, name, cfg, prefix, submit_url)

    def _op_render (self):
        txt   = "<h2>Libvirt options</h2>"

        table = TableProps()
        self.AddPropEntry (table, "mDNS Service Type", "%s!service_type" % (self._prefix), NOTE_SERVICE_TYPE)
        self.AddPropCheck (table, "Authenticate", "%s!authenticate" % (self._prefix), True, NOTE_AUTHENTICATE)
        self.AddPropCheck (table, "Read Only", "%s!read_only" % (self._prefix), True, NOTE_READ_ONLY)
        self.AddPropEntry (table, "XSL Stylesheet", "%s!xsl" % (self._prefix), NOTE_XSL)
        txt += self.Indent(table)

        return txt

    def _op_apply_changes (self, uri, post):
        checkboxes = ['authenticate', 'read_only']
        self.ApplyChangesPrefix (self._prefix, checkboxes, post)


class ModuleVirt (ModuleVirtBase):
    def __init__ (self, cfg, prefix, submit_url):
        ModuleVirtBase.__init__ (self, cfg, prefix, 'virt', submit_url)

    def _op_render (self):
        return ModuleVirtBase._op_render (self)

    def _op_apply_changes (self, uri, post):
        return ModuleVirtBase._op_apply_changes (self, uri, post)
