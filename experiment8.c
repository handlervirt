#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>

#include <libvirt/libvirt.h>

virConnectPtr conn = NULL; /* the hypervisor connection */

int main(int argc, char*argv[]) {
//    xenUnifiedPrivatePtr priv;
    char *xenbus_path, s[256], *params, *param_pool, *param_volume;
    unsigned int len = 0;
    virStoragePoolPtr pool;
    virStorageVolPtr volume;


    /* NULL means connect to local Xen hypervisor */
    conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL) {
        fprintf(stderr, "Failed to connect to hypervisor\n");
        return -1;
    }

    xenbus_path = getenv ("XENBUS_PATH");

    if (xenbus_path == NULL) {
        fprintf(stderr, "No XENBUS_PATH so fail\n");
        return -1;
    }

/*    priv = (xenUnifiedPrivatePtr) conn->privateData;

    if (priv->xshandle == NULL) {
        fprintf(stderr, "No Xen connection so fail\n");
        return -1;
    }

    snprintf(s, 255, "%s/params", xenbus_path);
    s[255] = '\0';

    params = xs_read(priv->xshandle, 0, &s[0], &len);*/

    param_pool   = &params[2];
    param_volume = strchr(param_pool, '/');

    if (param_volume == NULL) {
        fprintf(stderr, "No Volume found\n");
        return -1;
    }

    param_volume = '\0';
    param_volume++;

    printf("pool: %s, volume: %s\n", param_pool, param_volume);

    pool = virStoragePoolLookupByName(conn, param_pool);

    if (pool == NULL) {
        fprintf(stderr, "Pool doesn't exist\n");
        return -1;
    }

    volume = virStorageVolLookupByName(pool, param_volume);

    if (volume == NULL) {
        fprintf(stderr, "Volume doesn't exist\n");
        return -1;
    }

    printf("%\n\n%s",  virStorageVolGetKey(volume), virStorageVolGetXMLDesc(volume, 0));

    free(params);
    virStoragePoolFree(pool);
    virStorageVolFree(volume);
    virConnectClose(conn);

    return 0;
}
