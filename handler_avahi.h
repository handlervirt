/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CHEROKEE_HANDLER_AVAHI_H
#define CHEROKEE_HANDLER_AVAHI_H

#include <cherokee/cherokee.h>

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>

#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/thread-watch.h>

/* Data types
 */
typedef struct {
	cherokee_module_props_t  base;

    /* A table to store all our output  */
    cherokee_avl_r_t entries;

    AvahiClient *client;
    AvahiServiceBrowser *sb;
    AvahiThreadedPoll *threaded_poll;

    /* Configuration parameters */
    cherokee_buffer_t   service_type;
} cherokee_handler_avahi_props_t;

typedef struct {
    /* Shared structures */
	cherokee_handler_t       handler;

    /* A buffer is your output 'to be' */
	cherokee_buffer_t        buffer;

    enum {
        send_page,
        send_yourmother /* not advised but possible */
    } action; /* this could implement a state machine */
} cherokee_handler_avahi_t;

#define HDL_AVAHI(x)       ((cherokee_handler_avahi_t *)(x))
#define PROP_AVAHI(x)      ((cherokee_handler_avahi_props_t *)(x))
#define HDL_AVAHI_PROPS(x) (PROP_AVAHI(MODULE(x)->props))


/* Library init function
 */
void  PLUGIN_INIT_NAME(avahi)      (cherokee_plugin_loader_t *loader);
ret_t cherokee_handler_avahi_new   (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props);

/* virtual methods implementation
 */
ret_t cherokee_handler_avahi_init        (cherokee_handler_avahi_t *hdl);
ret_t cherokee_handler_avahi_free        (cherokee_handler_avahi_t *hdl);
ret_t cherokee_handler_avahi_step        (cherokee_handler_avahi_t *hdl, cherokee_buffer_t *buffer);
ret_t cherokee_handler_avahi_add_headers (cherokee_handler_avahi_t *hdl, cherokee_buffer_t *buffer);

static void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata);
static void resolve_callback(AvahiServiceResolver *r, AVAHI_GCC_UNUSED AvahiIfIndex interface, AVAHI_GCC_UNUSED AvahiProtocol protocol, AvahiResolverEvent event, const char *name, const char *type, const char *domain, const char *host_name, const AvahiAddress *address, uint16_t port, AvahiStringList *txt, AvahiLookupResultFlags flags, void* userdata);
static void browse_callback(AvahiServiceBrowser *b, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, const char *name, const char *type, const char *domain, AVAHI_GCC_UNUSED AvahiLookupResultFlags flags, void* userdata);

#endif /* CHEROKEE_HANDLER_AVAHI_H */
