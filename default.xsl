<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template match="/domain">
  <br />
  <img src="/images/{./os/type}.png" alt="{./os/type}" style="float: left;"/>
  <xsl:choose>
  <xsl:when test="@id &gt; 0">
  <a href="/virt/{translate(./name,'_', '/')}/virDomainShutdown" style="background-image: url('/images/power.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Shutdown</a>
  <a href="/virt/{translate(./name,'_', '/')}/virDomainReboot" style="background-image: url('/images/reboot.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Reboot</a>
  <a href="/virt/{translate(./name,'_', '/')}/virDomainSave" style="background-image: url('/images/restore.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Snapshot</a>
  <a href="/virt/{translate(./name,'_', '/')}/virDomainMigrate" style="background-image: url('/images/migrate.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Migrate</a>
  </xsl:when>
  <xsl:otherwise>
    <a href="/virt/{translate(./name,'_', '/')}/virDomainCreate" style="background-image: url('/images/power.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Create</a>

    <xsl:variable name="snapshots" select="concat('/virt/', translate(./name,'_', '/'), '/virDomainGetXMLSnapshots')" />
    <xsl:choose>
        <xsl:when test= "count(document($snapshots)/snapshots/*) &gt; 0">
            <a href="/virt/{translate(./name,'_', '/')}/virDomainRestore" style="background-image: url('/images/restore.png'); background-repeat: no-repeat; display: block; width: 45px; height: 47px; font-size: 0; float: left;">Snapshot</a>
        </xsl:when>
    </xsl:choose>
  </xsl:otherwise>
  </xsl:choose>

<h1 style="clear: both;" ><xsl:value-of select="translate(./name,'_', ' ')"/></h1>
  <img src="/virt/{translate(./name,'_', '/')}/virGraphLoad" alt="Load of {substring-after(./name,'_')}" />
  <img src="/virt/{translate(./name,'_', '/')}/virGraphInterface" alt="Traffic of {substring-after(./name,'_')}" />

</xsl:template>

<xsl:template match="domain">
    <xsl:choose>
        <xsl:when test="@status = 'running'">
            <xsl:variable name="color" select="'background-color: #0f0;'" />
            <a href="/virt/{translate(./name,'_', '/')}" style="float: left; margin: 0.2em; font-size: 1em; display: block; width: 7em; height: 7em; text-align: center; border: 1px black solid; vertical-align: middle; {$color} text-decoration: none;"><xsl:value-of select="substring-before(./name,'_')"/><br /><xsl:value-of select="substring-after(./name,'_')"/></a>
        </xsl:when>
        <xsl:when test="@status = 'running'">
            <xsl:variable name="color" select="'background-color: #000;'" /> 
            <a href="/virt/{translate(./name,'_', '/')}" style="float: left; margin: 0.2em; font-size: 1em; display: block; width: 7em; height: 7em; text-align: center; border: 1px black solid; vertical-align: middle; {$color} text-decoration: none;"><xsl:value-of select="substring-before(./name,'_')"/><br /><xsl:value-of select="substring-after(./name,'_')"/></a>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name="color" select="'background-color: #fef;'" />
            <a href="/virt/{translate(./name,'_', '/')}" style="float: left; margin: 0.2em; font-size: 1em; display: block; width: 7em; height: 7em; text-align: center; border: 1px black solid; vertical-align: middle; {$color} text-decoration: none;"><xsl:value-of select="substring-before(./name,'_')"/><br /><xsl:value-of select="substring-after(./name,'_')"/></a>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>VPS; Do you need an other argument to migrate?</title>
  <link rel="stylesheet" type="text/css" href="/css/default.css" />
</head>
<body>
  <img src="/images/vps.png" alt="VPS" />
  <xsl:apply-templates />
</body>
</html>
</xsl:template>
</xsl:stylesheet>
