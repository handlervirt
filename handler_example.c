/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <stefan@konink.de>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "handler_example.h"
#include <cherokee/cherokee.h>

/*
#include "util.h"
#include "connection.h"
#include "connection-protected.h"
#include "server.h"
#include "server-protected.h"
#include "plugin_loader.h"
#include "connection_info.h"
*/

/* Plug-in initialization
 *
 * In this function you can use any of these:
 * http_delete | http_get | http_post | http_put
 *
 * For a full list: cherokee_http_method_t
 *
 * It is what your handler to be implements.
 *
 */
PLUGIN_INFO_HANDLER_EASIEST_INIT (example, http_get);


/* Methods implementation
 */
static ret_t 
props_free (cherokee_handler_example_props_t *props)
{
	return cherokee_module_props_free_base (MODULE_PROPS(props));
}


ret_t 
cherokee_handler_example_configure (cherokee_config_node_t *conf, cherokee_server_t *srv, cherokee_module_props_t **_props)
{
	cherokee_list_t                      *i;
	cherokee_handler_example_props_t *props;

	if (*_props == NULL) {
		CHEROKEE_NEW_STRUCT (n, handler_example_props);

		cherokee_module_props_init_base (MODULE_PROPS(n), 
						 MODULE_PROPS_FREE(props_free));		
        
        /* Look at handler_example.h
         * This is an example of configuration.
         */
		n->example_config = false;

		*_props = MODULE_PROPS(n);
	}

	props = PROP_EXAMPLE(*_props);

	cherokee_config_node_foreach (i, conf) {
		cherokee_config_node_t *subconf = CONFIG_NODE(i);

		if (equal_buf_str (&subconf->key, "example_config")) {
			props->example_config = atoi(subconf->val.buf);
		} else {
			PRINT_MSG ("ERROR: Handler file: Unknown key: '%s'\n", subconf->key.buf);
			return ret_error;
		}
	}

	return ret_ok;
}

ret_t
cherokee_handler_example_new  (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props)
{
	ret_t ret;
	CHEROKEE_NEW_STRUCT (n, handler_example);
	
	/* Init the base class object
	 */
	cherokee_handler_init_base(HANDLER(n), cnt, HANDLER_PROPS(props), PLUGIN_INFO_HANDLER_PTR(example));
	   
	MODULE(n)->init         = (handler_func_init_t) cherokee_handler_example_init;
	MODULE(n)->free         = (module_func_free_t) cherokee_handler_example_free;
	HANDLER(n)->step        = (handler_func_step_t) cherokee_handler_example_step;
	HANDLER(n)->add_headers = (handler_func_add_headers_t) cherokee_handler_example_add_headers;

	HANDLER(n)->support = hsupport_length | hsupport_range;

	/* Init
	 */
	ret = cherokee_buffer_init (&n->buffer);
	if (unlikely(ret != ret_ok)) 
		return ret;

	ret = cherokee_buffer_ensure_size (&n->buffer, 4*1024);
	if (unlikely(ret != ret_ok)) 
		return ret;

	*hdl = HANDLER(n);
	return ret_ok;
}


ret_t 
cherokee_handler_example_free (cherokee_handler_example_t *hdl)
{
	cherokee_buffer_mrproper (&hdl->buffer);
	return ret_ok;
}

static void
example_build_page (cherokee_handler_example_t *hdl)
{
    ret_t              ret;
    cherokee_server_t *srv;
    cherokee_buffer_t *buf;

    /* Init
     */
    buf = &hdl->buffer;
    srv = HANDLER_SRV(hdl);

    /* Useful output
     */
    cherokee_buffer_add_str (buf, "Hello World");

    /* But we did configure something!
     */
    if (HDL_EXAMPLE_PROPS(hdl)->example_config) {
        cherokee_buffer_add_str (buf, "!!!");
    }
}

ret_t 
cherokee_handler_example_init (cherokee_handler_example_t *hdl)
{
	ret_t   ret;
	void   *param;
	cint_t  web_interface = 1;

	/* Build the page
	 */
	if (web_interface) {
		example_build_page (hdl);
	}

	hdl->action = send_page;
	
	return ret_ok;
}


ret_t 
cherokee_handler_example_step (cherokee_handler_example_t *hdl, cherokee_buffer_t *buffer)
{
	cherokee_buffer_add_buffer (buffer, &hdl->buffer);
	return ret_eof_have_data;
}


ret_t 
cherokee_handler_example_add_headers (cherokee_handler_example_t *hdl, cherokee_buffer_t *buffer)
{
	cherokee_buffer_add_va (buffer, "Content-Length: %d"CRLF, hdl->buffer.len);

	switch (hdl->action) {
    	case send_page:
    	default:
	    	cherokee_buffer_add_str (buffer, "Content-Type: text/html"CRLF);
		    break;
	}

	return ret_ok;
}
