#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

#include <libvirt/libvirt.h>


typedef struct offer OFFER;

struct offer {
    char *name;
    float cost;
    unsigned long memory;
    AvahiEntryGroup *group;
    short int reservate;
    OFFER *next;
};

OFFER *offers_head = NULL;
OFFER *offers_tail = NULL;

unsigned long reservate = 0;

static AvahiSimplePoll *simple_poll = NULL;

static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, AVAHI_GCC_UNUSED void *userdata);

int accept_tender(const char *name, unsigned long memory, AvahiClient *c) {
    OFFER * offer_new;
    char domain[254];
    if ((offer_new = (OFFER *) malloc(sizeof(OFFER))) == NULL) {
        return -1;
    }
    snprintf(domain, 254, "%s.%s", name, avahi_client_get_host_name_fqdn(c));
    offer_new->name = strdup(name);
    offer_new->memory = memory;
    offer_new->reservate = 1;
    offer_new->cost = 0.0f;
    reservate =+ memory;
    offer_new->group = avahi_entry_group_new(c, entry_group_callback, NULL);
    offer_new->next = NULL;

    /* TODO: enable givemetake only after domains are migrated! */
    avahi_entry_group_add_service(offer_new->group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, domain, "_tender._tcp", NULL, NULL, 651, "givemetake=1", NULL, NULL);
    avahi_entry_group_commit(offer_new->group);
    
    if (!offers_head)
        offers_head = offer_new;
    else
        offers_tail->next = offer_new;

    offers_tail = offer_new;
}

int offer_tender(const char *name, unsigned long memory, AvahiClient *c) {
    OFFER * offer_new;
    virConnectPtr conn = NULL; /* the hypervisor connection */

    unsigned long long free_memory;
    char domain[254], txt[254];

    /* NULL means connect to local Xen hypervisor */
    conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL) {
        fprintf(stderr, "Failed to connect to hypervisor\n");
        return -1;
    }

    free_memory = virNodeGetFreeMemory(conn) - reservate;
    virConnectClose(conn);

    if ((offer_new = (OFFER *) malloc(sizeof(OFFER))) == NULL) {
        return -1;
    }

    offer_new->cost = (float)memory / (float)free_memory;
    snprintf(domain, 254, "%s.%s", name, avahi_client_get_host_name_fqdn(c));
    snprintf(txt, 254, "cost=%f", offer_new->cost);

    printf("Puts hand up: %f\n", offer_new->cost);

    /* Here we assume that Avahi will guarantee uniqueness */
    offer_new->name = strdup(name);
    offer_new->memory = memory;
    offer_new->reservate = 0;
    offer_new->group = avahi_entry_group_new(c, entry_group_callback, NULL);
    offer_new->next = NULL;
    avahi_entry_group_add_service(offer_new->group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, domain, "_tender._tcp", NULL, NULL, 651, txt, NULL, NULL);
    avahi_entry_group_commit(offer_new->group);


    if (!offers_head)
        offers_head = offer_new;
    else
        offers_tail->next = offer_new;

    offers_tail = offer_new;
}

int pull_tender(const char *name) {  
    OFFER *offer_prev = NULL;
    OFFER *offer_find = offers_head;

    while (offer_find) {
        if (strcmp(offer_find->name, name) == 0) {
            if (offer_find->reservate == 1) reservate =- offer_find->memory;
            free(offer_find->name);
            avahi_entry_group_free(offer_find->group);
            if (!offer_prev)
                offers_head = offer_find->next;
            else
                offer_prev->next = offer_find->next;

            free(offer_find);
            return 0;
        } else {
            offer_prev = offer_find;
            offer_find = offer_find->next;
        }
    }
    return -1;
}



static void resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {

    assert(r);

    /* Called whenever a service has been resolved successfully or timed out */

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            fprintf(stderr, "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
            break;

        case AVAHI_RESOLVER_FOUND: {
            AvahiStringList *needle;
/*            char a[AVAHI_ADDRESS_STR_MAX], *t;
            
            fprintf(stderr, "Service '%s' of type '%s' in domain '%s':\n", name, type, domain);
            
            avahi_address_snprint(a, sizeof(a), address);
            t = avahi_string_list_to_string(txt);
            fprintf(stderr,
                    "\t%s:%u (%s)\n"
                    "\tTXT=%s\n"
                    "\tcookie is %u\n"
                    "\tis_local: %i\n"
                    "\tour_own: %i\n"
                    "\twide_area: %i\n"
                    "\tmulticast: %i\n"
                    "\tcached: %i\n",
                    host_name, port, a,
                    t,
                    avahi_string_list_get_service_cookie(txt),
                    !!(flags & AVAHI_LOOKUP_RESULT_LOCAL),
                    !!(flags & AVAHI_LOOKUP_RESULT_OUR_OWN),
                    !!(flags & AVAHI_LOOKUP_RESULT_WIDE_AREA),
                    !!(flags & AVAHI_LOOKUP_RESULT_MULTICAST),
                    !!(flags & AVAHI_LOOKUP_RESULT_CACHED));

            avahi_free(t);*/


            if ((needle = avahi_string_list_find (txt, "memory")) != NULL) {
                unsigned long int amount;
                char *memory;
                avahi_string_list_get_pair (needle, NULL, &memory, NULL);
                amount = strtoul(memory, NULL, 10);

                if (errno != ERANGE && amount > 0) {
                    if ((needle = avahi_string_list_find (txt, "domain")) != NULL) {
                        char *winner;
                        avahi_string_list_get_pair (needle, NULL, &winner, NULL);

                        if (strcmp(winner, avahi_client_get_host_name_fqdn(avahi_service_resolver_get_client(r))) == 0) {
                            accept_tender(name, amount, avahi_service_resolver_get_client(r));
                        }

                        avahi_free(winner);
                    } else {
                        offer_tender(name, amount, avahi_service_resolver_get_client(r));
                    }
                }
                
                avahi_free(memory);
            }
        }
    }

    avahi_service_resolver_free(r);
}

static void browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {
    
    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */

    switch (event) {
        case AVAHI_BROWSER_FAILURE:
            fprintf(stderr, "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_service_browser_free(b);
            return;

        case AVAHI_BROWSER_NEW: {
            AvahiClient *c = avahi_service_browser_get_client(b);
            fprintf(stderr, "(Browser) NEW: service '%s' of type '%s' in domain '%s'\n", name, type, domain);

            /* We ignore the returned resolver object. In the callback
               function we free it. If the server is terminated before
               the callback function is called the server will free
               the resolver for us. */

            if (!(avahi_service_resolver_new(c, interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, 0, resolve_callback, NULL)))
                fprintf(stderr, "Failed to resolve service '%s': %s\n", name, avahi_strerror(avahi_client_errno(c)));
            
            break;
        }

        case AVAHI_BROWSER_REMOVE:
            pull_tender(name);
            fprintf(stderr, "(Browser) REMOVE: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
            break;

        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
            fprintf(stderr, "(Browser) %s\n", event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
            break;
    }
}

static void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, AVAHI_GCC_UNUSED void *userdata) {
    switch (state) {
        case AVAHI_ENTRY_GROUP_ESTABLISHED :
            /* The entry group has been established successfully */
            break;

        case AVAHI_ENTRY_GROUP_COLLISION : {
            /* A service name collision with a remote service
             * happened. */
            break;
        }

        case AVAHI_ENTRY_GROUP_FAILURE :
            /* Some kind of failure happened while we were registering our services */
//            avahi_simple_poll_quit(simple_poll);
            break;

        case AVAHI_ENTRY_GROUP_UNCOMMITED:
        case AVAHI_ENTRY_GROUP_REGISTERING:
            ;
    }
}

static void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata) {

    /* Called whenever the client or server state changes */

    switch (state) {
        case AVAHI_CLIENT_S_RUNNING:
            /* The server has startup successfully and registered its host
             * name on the network, so it's time to create our services */
            
            if (!avahi_service_browser_new(c, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, "_offer._tcp", NULL, 0, browse_callback, NULL)) {
                fprintf(stderr, "Failed to create service browser: %s\n", avahi_strerror(avahi_client_errno(c)));
            }

            break;

        case AVAHI_CLIENT_FAILURE: {
            int error;
            AvahiClient *client;
            
            if (c) {
                fprintf(stderr, "Client failure: %s\n", avahi_strerror(avahi_client_errno(c)));
                avahi_client_free(c);
            }

            /* Allocate a new client */
            client = avahi_client_new(avahi_simple_poll_get(simple_poll), AVAHI_CLIENT_NO_FAIL, client_callback, NULL, &error);
            break;
        }

        case AVAHI_CLIENT_S_COLLISION:
            /* Let's drop our registered services. When the server is back
             * in AVAHI_SERVER_RUNNING state we will register them
             * again with the new host name. */

        case AVAHI_CLIENT_S_REGISTERING:
            /* The server records are now being established. This
             * might be caused by a host name change. We need to wait
             * for our own records to register until the host name is
             * properly esatblished. */
            break;

        case AVAHI_CLIENT_CONNECTING:
            ;
    }
}


int main(AVAHI_GCC_UNUSED int argc, AVAHI_GCC_UNUSED char*argv[]) {
    int error;
    int ret = 1;

    /* Allocate main loop object */
    if (!(simple_poll = avahi_simple_poll_new())) {
        fprintf(stderr, "Failed to create simple poll object.\n");
        goto fail;
    }

    client_callback(NULL, AVAHI_CLIENT_FAILURE, NULL);

    /* Run the main loop */
    avahi_simple_poll_loop(simple_poll);

    ret = 0;

fail:

    /* Cleanup things */

    if (simple_poll)
        avahi_simple_poll_free(simple_poll);

    return ret;
}
