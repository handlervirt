all: handler_example.c
	gcc -g -o create create.c
	gcc -g -Wall -lrrd -lavahi-client -o rrdsaver rrdsaver.c
#	gcc -g -lavahi-client -L/opt/libvirt/lib -lvirt -o domumdns domumdns.c
#	gcc -g -lavahi-client -L/opt/libvirt/lib -lvirt -o offer offer.c
#	gcc -g -fPIC -shared -I/opt/cherokee/include -o libplugin_example.so handler_example.c
#	gcc -g -fPIC -shared -I/opt/cherokee/include -lavahi-client -o libplugin_avahi.so handler_avahi.c
	gcc -DTRACE_ENABLED -D_GNU_SOURCE -I/usr/include/libxml2 -g -fPIC -shared -I/opt/cherokee/include -lrrd -lavahi-client -L/opt/domu/lib -lvirt -o libplugin_virt.so handler_clusterstats.c handler_virt.c handler_virt_dnsmasq.c handler_virt_domains.c handler_virt_tender.c
#	gcc -g -fPIC -shared -I/opt/cherokee/include -lavahi-client -o libplugin_clusterstats.so handler_clusterstats.c

install:
	cp *.so /opt/cherokee/lib/cherokee/.
	cp Module*.py /opt/cherokee/share/cherokee/admin/.
