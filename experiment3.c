#include <stdio.h>

int main() {
    FILE *fd;
    char mac[13];
    unsigned long int counter = 0;

    if ((fd = fopen("/tmp/maccounter", "r")) != NULL) {
        fread(mac, 12, sizeof(char), fd);
        mac[13] = '\0';
        counter = strtoul(mac, NULL, 16);
        counter++;
        fclose(fd);
    }

    snprintf(mac, 13, "%012X\n", counter);
    if ((fd = fopen("/tmp/maccounter", "w")) != NULL) {
        fwrite(mac, 12, sizeof(char), fd);
        fclose(fd);
    }
}
