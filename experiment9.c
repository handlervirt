#include <rrd.h>

int main() {
                char **calcpr  = NULL;
                int xsize, ysize;
                double ymin, ymax;
                FILE *fd = tmpfile();
                char *r_graph[] = { "rrdgraph",
               //                       "/home/skinkie/development/htdocs/rrdtest.png",
                                      "-a",
                                       "PNG",
                                       "-w",
                                       "600",
                                       "-h",
                                       "200",
                                    "DEF:cputime=/mnt/netapp/users/klant1/gentoo/cpuTime.rrd:cpuTime:AVERAGE:step=30", 
                                    "CDEF:cpuload=cputime,1000000000,/", 
                                    "LINE:cpuload#EE0000:cpuLoad" };

                rrd_graph(10, r_graph, &calcpr, &xsize, &ysize, fd, &ymin, &ymax);

                fclose(fd);
}
