#!/bin/sh

BASEPATH="\/mnt\/netapp\/users"
IMGPATH="\/mnt\/images"

find $BASEBATH -ctime 14 -name index.xml.deleted | while read i; do
    FILEDOMAIN=`echo $i | sed "s/$BASEPATH\/\(.*\)\/\(.*\)\/.*/\1_\2/g"`
    echo $FILEDOMAIN
    cat $i | grep "source file" | sed "s/.*file='$IMGPATH\/$FILEDOMAIN_\(.*\)'.*/\1/g" | while read j; do
        rm $IMGPATH/$FILEDOMAIN_$j
    done
    grep -v $FILEDOMAIN /mnt/netapp/ipservices/dv28.conf > /tmp/dv28.conf
    mv /tmp/dv28.conf /mnt/netapp/ipservices/dv28.conf
done


