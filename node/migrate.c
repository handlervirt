#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>
#include <avahi-common/strlst.h>
#include <avahi-common/thread-watch.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <libvirt/libvirt.h>

#include "../handler_virt_tender.h"

int migrate_domain(AvahiClient *client, AvahiThreadedPoll *threaded_poll, virDomainPtr dom) {
    int ret = -1;
    unsigned long memory;
    char *target;
    char destination[1024];
    virConnectPtr dconn;

    memory = virDomainGetMaxMemory(dom);
    target = create_tender(client, threaded_poll, virDomainGetName(dom), virDomainGetMaxMemory(dom));

    snprintf(destination, 1022, "xen://%s/", target);
    destination[1023] = '\0';
    free(target);

    dconn = virConnectOpen(destination);

    if (dconn) {
        virDomainPtr desDom;

        desDom = virDomainMigrate(dom, dconn, VIR_MIGRATE_LIVE, NULL, NULL, 0);

        if (desDom) {
            ret = 0;
            virDomainFree(desDom);
        }

        virConnectClose(dconn);
    }

    return ret;
}
