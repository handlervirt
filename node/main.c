#include <stdio.h>
#include <stdlib.h>

#include <avahi-client/client.h>
#include <avahi-common/simple-watch.h>
#include <signal.h>
#include <unistd.h>

#include "node.h"

void terminate(int sig) {
    pid_t  pid;

    if (offerServiceBrowser) {
        avahi_service_browser_free(offerServiceBrowser);
        offerServiceBrowser = NULL;
    }

    pid = fork();

    if (pid == 0) {
        migrate_everything();
//        exit(0);
    }
}

int main(AVAHI_GCC_UNUSED int argc, AVAHI_GCC_UNUSED char*argv[]) {
    /* Allocate main loop object */
    if (!(simple_poll = avahi_simple_poll_new())) {
        fprintf(stderr, "Failed to create simple poll object.\n");
        goto fail;
    }

    /* The client that is going to do the work */
    client_callback(NULL, AVAHI_CLIENT_FAILURE, NULL);

    signal(SIGTERM, terminate);

    /* Run the main loop */
    avahi_simple_poll_loop(simple_poll);

fail:
    /* Cleanup things */
    if (simple_poll)
        avahi_simple_poll_free(simple_poll);

    exit(EXIT_SUCCESS);
}
