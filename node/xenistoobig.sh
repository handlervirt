#!/bin/sh
while [ 1 ]; do
        if [ `ps aux | grep "xend start" | awk '{ SUM += $6 } END { print SUM }'` -ge 120000 ]; then
                echo "Xen is too big!"
                killall xend; /etc/init.d/xend zap; /etc/init.d/xend start
        fi
        sleep 600
done
