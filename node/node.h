#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/thread-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>

#include <libvirt/libvirt.h>

AvahiSimplePoll *simple_poll;
AvahiServiceBrowser *offerServiceBrowser;

#define POLL 5

void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata);
void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, AVAHI_GCC_UNUSED void *userdata);
void parse_tender(AvahiServiceResolver *r, const char *name, AvahiStringList *txt);
void create_services(AvahiClient *c);
int pull_tender(const char *name);
void remove_everything();
void pull_all();
int has_tender(const char *name);
int migrate_domain(AvahiClient *client, AvahiThreadedPoll *threaded_poll, virDomainPtr dom);
void migrate_everything();
