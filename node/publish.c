#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>
#include <avahi-common/strlst.h>
#include <avahi-common/thread-watch.h>

#include <libvirt/libvirt.h>

#include "node.h"

struct list_el {
    unsigned int domainid;
    AvahiEntryGroup *group;
    unsigned short keep;
};

typedef struct list_el domu;

domu * domus = NULL; /* list of domains */
unsigned int domu_count = 0;


static AvahiStringList * domainProperties(virDomainPtr thisDomain) {
    AvahiStringList *new = NULL;
    // avahi_string_list_new_from_array (NULL, 0);
    virDomainInfo info;
    virDomainInterfaceStatsStruct stats;
    virDomainGetInfo(thisDomain, &info);
    new = avahi_string_list_add_printf(new, "maxMem=%lu", info.maxMem);
    new = avahi_string_list_add_printf(new, "memory=%lu", info.memory);
    new = avahi_string_list_add_printf(new, "cpuTime=%llu", info.cpuTime);

    /* TODO: multiple network interfaces :( */
    char vifname[16];
    snprintf(vifname, 14, "vif%d.0", virDomainGetID(thisDomain));
    vifname[15] = '\0';
    unsigned int no = 0;

    virDomainInterfaceStats(thisDomain, vifname, &stats, sizeof(stats));

    new = avahi_string_list_add_printf(new, "interfaces=eth%d", no);
    new = avahi_string_list_add_printf(new, "interface_eth%d_rxbytes=%lld",   no, stats.rx_bytes);
    new = avahi_string_list_add_printf(new, "interface_eth%d_rxpackets=%lld", no, stats.rx_packets);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_rxerrs=%lld",    no, stats.rx_errs);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_rxdrop=%lld",    no, stats.rx_drop);
    new = avahi_string_list_add_printf(new, "interface_eth%d_txbytes=%lld",   no, stats.tx_bytes);
    new = avahi_string_list_add_printf(new, "interface_eth%d_txpackets=%lld", no, stats.tx_packets);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_txerrs=%lld",    no, stats.tx_errs);
//    new = avahi_string_list_add_printf(new, "interface_eth%d_txdrop=%lld",    no, stats.tx_drop);

    return new;
}

void remove_everything() {
    if (domus) {
        int i;
        for (i = 0; i < domu_count; i++) {
            if (domus[i].group) {
                avahi_entry_group_free(domus[i].group);
                domus[i].group = NULL;
            }
        }
        free(domus);
        domus = NULL;
        domu_count = 0;
    }
}

void migrate_everything() {
    if (domus) {
        virConnectPtr conn;
        conn = virConnectOpen(NULL);

        if (conn) {
            int i;
            AvahiThreadedPoll *threaded_poll = NULL;
            AvahiClient *client = NULL;

            if ((threaded_poll = avahi_threaded_poll_new())) {
                if ((client = avahi_client_new(avahi_threaded_poll_get(threaded_poll), 0, NULL, NULL, NULL)))
                    avahi_threaded_poll_start(threaded_poll);
            }

            for (i = 0; i < domu_count; i++) {
                virDomainPtr dom;
                dom = virDomainLookupByID (conn, domus[i].domainid);

                if (dom) {
                    if (client == NULL || migrate_domain(client, threaded_poll, dom) != 0) {
                        virDomainShutdown(dom); 
                        virDomainFree(dom);
                    }
                }

            }
            
            if (threaded_poll) {
                if (client) {
                    avahi_threaded_poll_stop(threaded_poll);
                    avahi_client_free(client);
                }

                avahi_threaded_poll_free(threaded_poll);
            }

            virConnectClose(conn);
        }
    }
}

static void entry_group_callback_publish(AvahiEntryGroup *g, AvahiEntryGroupState state, void *userdata) {
    domu *this = userdata;

    switch (state) {
        case AVAHI_ENTRY_GROUP_ESTABLISHED:
            this->group = g;
            break;

        case AVAHI_ENTRY_GROUP_FAILURE:
        case AVAHI_ENTRY_GROUP_COLLISION:
            fprintf(stderr, "Collision\n");
            avahi_entry_group_free(g);
            this->group = NULL;
            break;

        default:
            break;
    }
}


void create_services(AvahiClient *c) {
    virConnectPtr conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL)
        return;
    int maxid = virConnectNumOfDomains(conn);
    if (maxid > 1) { /* ignore dom0 */
        int *ids = (int *) malloc(sizeof(int) * maxid);
        if ((maxid = virConnectListDomains(conn, &ids[0], maxid)) < 0) {
            // error
        } else {
            int i;
            unsigned int domu_count_new = (maxid - 1);
            domu * domus_old = domus;
            domus = (domu *) malloc(sizeof(domu) * domu_count_new);
            const char *type = "_domu._tcp";

            for (i = 0; i < domu_count_new; i++) {
                int j;
                domus[i].group = NULL;
                domus[i].domainid = ids[i+1];
                domus[i].keep = 0;

//                fprintf(stderr, "Looking for %d\n", domus[i].domainid);

                for (j = 0; j < domu_count; j++) {
//                    fprintf(stderr, "--> %d\n", domus_old[j].domainid);
                    if (ids[i+1] == domus_old[j].domainid) {
//                        fprintf(stderr, "got it\n");
                        virDomainPtr thisDomain = virDomainLookupByID(conn, domus[i].domainid);
                        if (thisDomain) {
                            const char *name;
                            name = virDomainGetName(thisDomain);
                            if (name && domus_old[j].group) {
                                domus[i].group = domus_old[j].group;
                                AvahiStringList *domainProps;
                                domainProps = domainProperties(thisDomain);
                                domus_old[j].keep = 1;
                                avahi_entry_group_update_service_txt_strlst(domus[i].group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, name, type, NULL, domainProps);
                                avahi_string_list_free(domainProps);
                            }
                            virDomainFree(thisDomain);
                        }
                    }
                }
                if (i > domu_count || domus[i].group == NULL) {
                    const char *name;
                    AvahiStringList *domainProps;
                    virDomainPtr thisDomain = virDomainLookupByID(conn, ids[i+1]);
                    if (thisDomain) {
                        AvahiEntryGroup *group;
                        group = avahi_entry_group_new(c, entry_group_callback_publish, &domus[i]);
                        domainProps = domainProperties(thisDomain);
                        name = virDomainGetName(thisDomain);

                        if (name) {
                            avahi_entry_group_add_service_strlst(group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, name, type, NULL, NULL, 651, domainProps);
                            avahi_entry_group_commit(group);
                            domus[i].keep = 1;
                        } else {
                            domus[i].keep = 0;
                        }

                        avahi_string_list_free(domainProps);
                        virDomainFree(thisDomain);
                    }
                }
            }

            for (i = 0; i < domu_count; i++) {
                // fprintf(stderr, "id: %d, keep: %d\n", domus_old[i].domainid, domus_old[i].keep);
                if (domus_old[i].keep == 0 && domus_old[i].group != NULL) {
                    avahi_entry_group_free(domus_old[i].group);
                    domus_old[i].group = NULL;
                }
            }

            free(domus_old);
            domu_count = domu_count_new;
        }
        free(ids);
    } else {
        remove_everything();
    }
    virConnectClose(conn);
    //fprintf(stderr, "exit: %s\n", __func__);
}

