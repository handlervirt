#include <stdio.h>
#include <time.h>
#include <errno.h>

#include <avahi-client/client.h>
#include <avahi-client/lookup.h>
#include <avahi-common/simple-watch.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

#include "node.h"

static void resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {

    assert(r);

    /* Called whenever a service has been resolved successfully or timed out */

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            fprintf(stderr, "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
            break;

        case AVAHI_RESOLVER_FOUND: 
            parse_tender(r, name, txt);
            break;
    }

    avahi_service_resolver_free(r);
}

static void modify_callback(AvahiTimeout *e, void *userdata) {
    AvahiClient *client = userdata;

    /* If the server is currently running, we need to remove our
     * service and create it anew */

    if (avahi_client_get_state(client) != AVAHI_CLIENT_FAILURE) {
            /* And create them again with the new name */
            struct timeval tv;

            create_services(client);

            avahi_simple_poll_get(simple_poll)->timeout_update(e, avahi_elapse_time(&tv, 1000*POLL, 0));
    }
}

static void browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {

    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */

    switch (event) {
        case AVAHI_BROWSER_FAILURE:
            pull_tender(name);
            fprintf(stderr, "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_service_browser_free(b);
            return;

        case AVAHI_BROWSER_NEW: {
                                    AvahiClient *c = avahi_service_browser_get_client(b);
                                    fprintf(stderr, "(Browser) NEW: service '%s' of type '%s' in domain '%s'\n", name, type, domain);

                                    /* We ignore the returned resolver object. In the callback
                                       function we free it. If the server is terminated before
                                       the callback function is called the server will free
                                       the resolver for us. */

                                    if (!(avahi_service_resolver_new(c, interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, 0, resolve_callback, NULL)))
                                        fprintf(stderr, "Failed to resolve service '%s': %s\n", name, avahi_strerror(avahi_client_errno(c)));

                                    break;
                                }

        case AVAHI_BROWSER_REMOVE:
                                pull_tender(name);
                                fprintf(stderr, "(Browser) REMOVE: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
                                break;

        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
                                fprintf(stderr, "(Browser) %s\n", event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
                                break;
    }
}


void client_callback(AvahiClient *c, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata) {
    /* Called whenever the client or server state changes */

    switch (state) {
        case AVAHI_CLIENT_S_RUNNING: {
                                         /* The server has startup successfully and registered its host
                                          * name on the network, so it's time to create our services */
                                         struct timeval tv;

                                         if (! (offerServiceBrowser = avahi_service_browser_new(c, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, "_offer._tcp", NULL, 0, browse_callback, NULL))) {
                                             fprintf(stderr, "Failed to create service browser: %s\n", avahi_strerror(avahi_client_errno(c)));
                                         }

                                         /* After 10s do some weird modification to the service */
                                         avahi_simple_poll_get(simple_poll)->timeout_new(
                                                 avahi_simple_poll_get(simple_poll),
                                                 avahi_elapse_time(&tv, 1000*POLL, 0),
                                                 modify_callback,
                                                 c);

                                         break;
                                     }

        case AVAHI_CLIENT_FAILURE: {
            int error;
            AvahiClient *client;

            if (c) {
                fprintf(stderr, "Client failure: %s\n", avahi_strerror(avahi_client_errno(c)));
                avahi_client_free(c);
            }
            remove_everything();
            pull_all();

            /* Allocate a new client */
            client = avahi_client_new(avahi_simple_poll_get(simple_poll), AVAHI_CLIENT_NO_FAIL, client_callback, NULL, &error);
            break;
        }

        case AVAHI_CLIENT_S_COLLISION:
            /* Let's drop our registered services. When the server is back
             * in AVAHI_SERVER_RUNNING state we will register them
             * again with the new host name. */

        case AVAHI_CLIENT_S_REGISTERING:
            /* The server records are now being established. This
             * might be caused by a host name change. We need to wait
             * for our own records to register until the host name is
             * properly esatblished. */
            break;

        case AVAHI_CLIENT_CONNECTING:
            ;
    }
}

void entry_group_callback(AvahiEntryGroup *g, AvahiEntryGroupState state, AVAHI_GCC_UNUSED void *userdata) {
    switch (state) {
        case AVAHI_ENTRY_GROUP_ESTABLISHED :
            /* The entry group has been established successfully */
            break;   

        case AVAHI_ENTRY_GROUP_COLLISION : {
            /* A service name collision with a remote service
             * happened. */
            break;
        }

        case AVAHI_ENTRY_GROUP_FAILURE :
            /* Some kind of failure happened while we were registering our services */
//            avahi_simple_poll_quit(simple_poll);
            break;

        case AVAHI_ENTRY_GROUP_UNCOMMITED:
        case AVAHI_ENTRY_GROUP_REGISTERING:
            ;
    }
}

