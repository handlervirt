#include <stdio.h>
#include "node.h"

#include <avahi-client/client.h>
#include <avahi-client/publish.h>  
#include <avahi-client/lookup.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>  
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

#include <libvirt/libvirt.h>

typedef struct offer OFFER;
struct offer {
    char *name;
    float cost;
    unsigned long memory;
    AvahiEntryGroup *group;
    short int reservate;
    OFFER *next;
};

OFFER *offers_head = NULL;
OFFER *offers_tail = NULL;

unsigned long reservate = 0;

static int accept_tender(const char *name, unsigned long memory, AvahiClient *c) {
    OFFER * offer_new;
    char domain[254];
    if ((offer_new = (OFFER *) malloc(sizeof(OFFER))) == NULL) {
        return -1;
    }
    snprintf(domain, 254, "%s.%s", name, avahi_client_get_host_name_fqdn(c));
    offer_new->name = strdup(name);
    offer_new->memory = memory;
    offer_new->reservate = 1;
    offer_new->cost = 0.0f;
    reservate =+ memory;
    offer_new->group = avahi_entry_group_new(c, entry_group_callback, NULL);
    offer_new->next = NULL;

    /* TODO: enable givemetake only after domains are migrated! */
    avahi_entry_group_add_service(offer_new->group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, domain, "_tender._tcp", NULL, NULL, 651, "givemetake=1", NULL, NULL);
    avahi_entry_group_commit(offer_new->group);

    if (!offers_head)
        offers_head = offer_new;
    else
        offers_tail->next = offer_new;

    offers_tail = offer_new;

    return 0;
}

static int offer_tender(const char *name, unsigned long memory, AvahiClient *c) {
    OFFER * offer_new;
    virConnectPtr conn = NULL; /* the hypervisor connection */

    unsigned long long free_memory;
    char domain[254], txt[254];

    /* NULL means connect to local Xen hypervisor */
    conn = virConnectOpenReadOnly(NULL);
    if (conn == NULL) {
        fprintf(stderr, "Failed to connect to hypervisor\n");
        return -1;
    }

    free_memory = virNodeGetFreeMemory(conn) - reservate;
    virConnectClose(conn);

    if ((offer_new = (OFFER *) malloc(sizeof(OFFER))) == NULL) {
        return -1;
    }

    offer_new->cost = (float)memory / (float)free_memory;
    snprintf(domain, 254, "%s.%s", name, avahi_client_get_host_name_fqdn(c));
    snprintf(txt, 254, "cost=%f", offer_new->cost);

    printf("Puts hand up: %f\n", offer_new->cost);

    /* Here we assume that Avahi will guarantee uniqueness */
    offer_new->name = strdup(name);
    offer_new->memory = memory;
    offer_new->reservate = 0;
    offer_new->group = avahi_entry_group_new(c, entry_group_callback, NULL);
    offer_new->next = NULL;
    avahi_entry_group_add_service(offer_new->group, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC, 0, domain, "_tender._tcp", NULL, NULL, 651, txt, NULL, NULL);
    avahi_entry_group_commit(offer_new->group);


    if (!offers_head)
        offers_head = offer_new;
    else
        offers_tail->next = offer_new;

    offers_tail = offer_new;

    return 0;
}



void pull_all() {
    OFFER *offer_prev = NULL;
    OFFER *offer_find = offers_head;

    while (offer_find) {
        if (offer_find->reservate == 1) reservate =- offer_find->memory;
        free(offer_find->name);
        avahi_entry_group_free(offer_find->group);
        if (!offer_prev)
            offers_head = offer_find->next;
        else
            offer_prev->next = offer_find->next;
        free(offer_find);
    }
}

int has_tender(const char *name) {
    OFFER *offer_prev = NULL;
    OFFER *offer_find = offers_head;

    while (offer_find) {
        if (strcmp(offer_find->name, name) == 0) {
            return 0;
        } else {
            offer_prev = offer_find;
            offer_find = offer_find->next;
        }
    }
    return -1;
}

int pull_tender(const char *name) {
    OFFER *offer_prev = NULL;
    OFFER *offer_find = offers_head;

    while (offer_find) {
        if (strcmp(offer_find->name, name) == 0) {
            if (offer_find->reservate == 1) reservate =- offer_find->memory;
            free(offer_find->name);
            avahi_entry_group_free(offer_find->group);
            if (!offer_prev)
                offers_head = offer_find->next;
            else
                offer_prev->next = offer_find->next;

            free(offer_find);
            return 0;
        } else {
            offer_prev = offer_find;
            offer_find = offer_find->next;
        }
    }
    return -1;
}

void parse_tender(AvahiServiceResolver *r, const char *name, AvahiStringList *txt) {
    AvahiStringList *needle;
    if ((needle = avahi_string_list_find (txt, "memory")) != NULL) {
        unsigned long int amount;
        char *memory;
        avahi_string_list_get_pair (needle, NULL, &memory, NULL);
        amount = strtoul(memory, NULL, 10);

        if (errno != ERANGE && amount > 0) {
            if ((needle = avahi_string_list_find (txt, "domain")) != NULL) {
                char *winner;
                avahi_string_list_get_pair (needle, NULL, &winner, NULL);

                if (strcmp(winner, avahi_client_get_host_name_fqdn(avahi_service_resolver_get_client(r))) == 0) {
                    accept_tender(name, amount, avahi_service_resolver_get_client(r));
                }

                avahi_free(winner);
            } else {
                if (has_tender(name) == -1)
                    offer_tender(name, amount, avahi_service_resolver_get_client(r));
            }
        }

        avahi_free(memory);
    }
}
