from Form import *
from Table import *
from Module import *
from validations import *

NOTE_SERVICE_TYPE   = 'What service type should we browse on the network for.'

class ModuleAvahiBase (Module, FormHelper):
    PROPERTIES = [
        'service_type'
    ]

    def __init__ (self, cfg, prefix, name, submit_url):
        FormHelper.__init__ (self, name, cfg)
        Module.__init__ (self, name, cfg, prefix, submit_url)

    def _op_render (self):
        txt   = "<h2>Avahi options</h2>"

        table = TableProps()
        self.AddPropEntry (table, "mDNS Service Type", "%s!service_type" % (self._prefix), NOTE_SERVICE_TYPE)
        txt += self.Indent(table)

        return txt

    def _op_apply_changes (self, uri, post):
        self.ApplyChangesPrefix (self._prefix, [], post)
                                

class ModuleAvahi (ModuleAvahiBase):
    def __init__ (self, cfg, prefix, submit_url):
        ModuleAvahiBase.__init__ (self, cfg, prefix, 'avahi', submit_url)

    def _op_render (self):
        return ModuleAvahiBase._op_render (self)

    def _op_apply_changes (self, uri, post):
        return ModuleAvahiBase._op_apply_changes (self, uri, post)
